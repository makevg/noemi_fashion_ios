//
//  CLLocation+Distance.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocation {
    
    func distanceInKilometers(from location: CLLocation) -> Float {
        return Float(distance(from: location) / 1000.0)
    }
    
    func distanceInKilometersString(from location: CLLocation) -> String {
        let distance = distanceInKilometers(from: location)
        return "\(distance.oneDigitAfterPointString())"
    }
    
}
