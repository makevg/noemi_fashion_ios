//
//  BaseCollectionViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 03.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
 
    // MAR: - Properties
    
    class var identifier: String {
        return String(describing: self)
    }
    
    class var cellHeight: CGFloat {
        return 44
    }
    
}
