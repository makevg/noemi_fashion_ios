//
//  UIImagePickerController+Helper.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Photos

extension UIImagePickerController {
    
	static func obtainPermission(for mediaSourceType: UIImagePickerController.SourceType, successHandler: @escaping () -> (), failureHandler: @escaping () -> ()) {
        if mediaSourceType == .photoLibrary || mediaSourceType == .savedPhotosAlbum {
            PHPhotoLibrary.requestAuthorization({ status in
                switch status {
                case .authorized:
                    DispatchQueue.main.async {
                        successHandler()
                    }
                case .restricted, .denied:
                    DispatchQueue.main.async {
                        failureHandler()
                    }
                default:
                    break
                }
            })
        }
        else if mediaSourceType == .camera {
					let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch authStatus {
            case .authorized:
                DispatchQueue.main.async {
                    successHandler()
                }
            case .notDetermined:
							AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                    if granted {
                        DispatchQueue.main.async {
                            successHandler()
                        }
                    } else {
                        DispatchQueue.main.async {
                            failureHandler()
                        }
                    }
                })
            case .denied, .restricted:
                DispatchQueue.main.async {
                    failureHandler()
                }
            }
        }
    }
    
}
