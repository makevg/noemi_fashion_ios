//
//  NotAuthorizedView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 07.07.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol NotAuthorizedViewDelegate: class {
    func notAuthorizedViewSignUpButtonTapped()
}

class NotAuthorizedView: UIView {

    // MARK: - Properties
    
    @IBOutlet fileprivate var mainView: UIView!
    
    weak var delegate: NotAuthorizedViewDelegate?
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - UI Actions
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        delegate?.notAuthorizedViewSignUpButtonTapped()
    }
    
    // MARK: - Private
    
    private func commonInit() {
        let key = String(describing: NotAuthorizedView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }
    
}
