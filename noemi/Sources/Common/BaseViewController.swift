//
//  BaseViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 12.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    // MARK: - Properties
    
    fileprivate var userAuthorized: Bool {
        let tokenIsEmpty = AuthorizationManager.shared.accessToken.isEmpty
        return !tokenIsEmpty
    }
    
    fileprivate var currentViewController: UIViewController?
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavBar()
    }
    
    // MARK: - Public
    
    func updateFunctionalMode() {
			let appDelegate = UIApplication.shared.delegate as! AppDelegate
			
        if userAuthorized {
					if #available(iOS 10.0, *) {
						appDelegate.registerForPushNotifications()
					}
					
            let createLookViewController = UIStoryboard.Looks.createLookViewController
            createLookViewController.delegate = self
            showInNavigationController(createLookViewController)
        } else {
					if #available(iOS 10.0, *) {
						appDelegate.unregisterForPushNotifications()
					}
					
            let controller = UIStoryboard.Main.tabBarController
            setRootController(controller)
        }
    }
    
    func setRootController(_ controller: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = controller
    }
    
    func add(asChildViewController viewController: UIViewController) {
        if currentViewController != nil {
            remove(asChildViewController: currentViewController!)
        }
        
        currentViewController = viewController
        
			addChild(viewController)
        view.addSubview(viewController.view)
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
			viewController.didMove(toParent: self)
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension BaseViewController {
    
    fileprivate func configureNavBar() {
        prepareTitleView()
        prepareScanButton()
    }
    
    fileprivate func remove(asChildViewController viewController: UIViewController) {
			viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
			viewController.removeFromParent()
    }
    
    private func prepareTitleView() {
        let titleView = TitleView.titleView
        titleView.delegate = self
        navigationItem.titleView = titleView
    }
    
    private func prepareScanButton() {
        let button = UIBarButtonItem(image: UIImage(named: "Scan"),
                                     style: .plain,
                                     target: self,
                                     action: #selector(scanButtonAction))
        navigationItem.rightBarButtonItem = button
    }
    
    fileprivate func showInNavigationController(_ controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        present(navigationController, animated: true, completion: nil)
    }
    
    // MARK: - UI Actions
    
    @objc
    private func scanButtonAction() {
		let QRScannerController = UIStoryboard.QRScanner.initialController
		present(QRScannerController, animated: true, completion: nil)
    }
    
    @objc
    private func titleViewAction() {
        let createLookViewController = UIStoryboard.Looks.createLookViewController
        showInNavigationController(createLookViewController)
    }
    
}

// MARK: - CreateLookViewControllerDelegate

extension BaseViewController: CreateLookViewControllerDelegate {
    
    func createLookControllerClosed() {
        let controller = UIStoryboard.Main.tabBarController
        setRootController(controller)
    }
    
}

// MARK: - TitleViewDelegate

extension BaseViewController: TitleViewDelegate {
    
    func titleViewTapped() {
        if userAuthorized {
            let createLookViewController = UIStoryboard.Looks.createLookViewController
            showInNavigationController(createLookViewController)
        }
    }
}
