//
//  UIImage+Helper.swift
//  noemi
//
//  Created by Maximychev Evgeny on 30.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

extension UIImage {
    
    func addText(_ text: String) -> UIImage {
        var textFont = UIFont.boldSystemFont(ofSize: 35)
        var textSize = text.sizeOfString(constrainedToWidth: Double(self.size.width), font: textFont)
        
        while textSize.height > 250 {
            textFont = UIFont.systemFont(ofSize: textFont.pointSize - 1)
            textSize = text.sizeOfString(constrainedToWidth: Double(self.size.width), font: textFont)
        }
        
        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: self.size))
        imageView.image = self
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: imageView.frame.width, height: textSize.height))
        label.text = text
        label.font = textFont
        label.numberOfLines = 0
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = UIColor(red: 0.654, green: 0.654, blue: 0.654, alpha: 0.4)
        
        imageView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: imageView.leadingAnchor).isActive = true
        label.trailingAnchor.constraint(equalTo: imageView.trailingAnchor).isActive = true
        label.bottomAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, true, 0)
        imageView.drawHierarchy(in: imageView.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func resizedImage() -> UIImage {
        var actualHeight = self.size.height
        var actualWidth = self.size.width
        let maxHeight: CGFloat = 1000
        let maxWidth: CGFloat = 1000
        var imgRatio = actualWidth/actualHeight
        let maxRatio = maxWidth/maxHeight
        let compressionQuality: CGFloat = 0.5
    
        if (actualHeight > maxHeight) || (actualWidth > maxWidth) {
            if imgRatio < maxRatio {
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
				let imageData = image!.jpegData(compressionQuality: compressionQuality)!
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData)!
    }
   
    
}

extension String {
    
    func sizeOfString(constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return self.boundingRect(with: CGSize(width: width, height: Double.greatestFiniteMagnitude),
                                           options: NSStringDrawingOptions.usesLineFragmentOrigin,
																					 attributes: [NSAttributedString.Key.font: font],
                                           context: nil).size
    }
    
}
