//
//  UIAlertController+Helpers.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit


extension UIAlertController {
    
    class func showAlert(with title: String, message: String, in controller: UIViewController) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ок", style: .default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    class func showErrorAlert(in controller: UIViewController) {
        showAlert(with: "Ошибка", message: "К сожалению, что-то пошло не так. Попробуйте позже.", in: controller)
    }
    
    class func showOpenSettingsAlert(with message: String, in controller: UIViewController) {
        let alertController = UIAlertController (title: "Внимание", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "", style: .default, handler: { _ in
					UrlOpener.open(UIApplication.openSettingsURLString)
        }))
        alertController.addAction(UIAlertAction(title: "Отменить", style: .default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
}
