//
//  InputAccessoryView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol InputAccessoryViewDelegate: class {
    func inputAccessoryViewDidTapDoneButton(view: InputAccessoryView)
}

class InputAccessoryView: UIView {
    
    // MARK: - Public properties
    
    weak var delegate: InputAccessoryViewDelegate?
    
    // MARK: - Private properties
    
    @IBOutlet var mainView: UIView!
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - Actions
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        delegate?.inputAccessoryViewDidTapDoneButton(view: self)
    }

}

// MARK: - Extensions

// MARK: - Private

extension InputAccessoryView {
    
    fileprivate func commonInit() {
        let key = String(describing: InputAccessoryView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }
    
}
