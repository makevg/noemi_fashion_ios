//
//  BaseTableViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    // MAR: - Properties
    
    class var identifier: String {
        return String(describing: self)
    }
    
    class var cellHeight: CGFloat {
        return 44
    }

}
