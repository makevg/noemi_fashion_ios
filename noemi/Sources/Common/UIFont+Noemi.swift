//
//  UIFont+Noemi.swift
//  noemi
//
//  Created by Maximychev Evgeny on 05.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

extension UIFont {
    
    static func noemiFont(with size: CGFloat) -> UIFont {
        return UIFont(name: "MicrogrammaD-MediExte", size: size)!
    }
    
}
