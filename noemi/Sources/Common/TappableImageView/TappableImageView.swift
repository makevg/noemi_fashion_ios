//
//  TappableImageView.swift
//  noemi
//
//  Created by Максимычев Е.О. on 17/10/2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class TappableImageView: UIImageView {
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		configure()
	}
	
	// MARK: - Internal
	
	func didTapImage() {
		showImageViewer()
	}
	
	// MARK: - Private
	
	private func configure() {
		let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(_:)))
		addGestureRecognizer(gesture)
		isUserInteractionEnabled = true
	}
	
	private func showImageViewer() {
		guard let imageForPresenting = image else { return }
		
		let imageViewer = UIStoryboard.ImageViewer.imageViewerController
		imageViewer.configure(with: imageForPresenting)
		UIApplication.shared.keyWindow?.rootViewController?.present(imageViewer, animated: true, completion: nil)
	}
	
	@objc func imageTapped(_ sender: UITapGestureRecognizer) {
		didTapImage()
	}
	
}
