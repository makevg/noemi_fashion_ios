//
//  DataPickerViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol DataPickerCotrollerDelegate: class {
    func dataPickerCotroller(controller: DataPickerViewController, select data: String)
}

class DataPickerViewController: UIViewController {
    
    // MARK: - Properties
    
    var data = [String]()
    weak var delegate: DataPickerCotrollerDelegate?
    
    @IBOutlet fileprivate weak var mainPickerView: UIView!
    @IBOutlet fileprivate weak var picker: UIPickerView!
    @IBOutlet fileprivate weak var mainPickerViewBottomConstraint: NSLayoutConstraint!
    
    fileprivate let animationDuration = 0.3
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showMainPickerView()
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension DataPickerViewController {
    
    fileprivate func configureController() {
        picker.dataSource = self
        picker.delegate = self
    }
    
    fileprivate func showMainPickerView() {
        UIView.animate(withDuration: animationDuration) { [weak self] in
            guard let this = self else {
                return
            }
            
            this.mainPickerViewBottomConstraint.constant = 0
            this.view.layoutIfNeeded()
        }
    }
    
    fileprivate func hideMainPickerView(completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: animationDuration,
                       animations: { [weak self] in
                        guard let this = self else {
                            return
                        }
                        
                        let mainPickerViewHeight = this.mainPickerView.frame.height
                        this.mainPickerViewBottomConstraint.constant = -mainPickerViewHeight
                        this.view.layoutIfNeeded()
            },
                       completion: completion)
    }
    
    fileprivate func close() {
        hideMainPickerView { [weak self] (true) in
            guard let this = self else {
                return
            }
            
            this.dismiss(animated: true, completion: nil)
        }
    }
    
}

// MARK: - UI Actions

extension DataPickerViewController {
    
    @IBAction func viewTapped(_ sender: Any) {
        close()
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        close()
    }
    
    @IBAction func selectButtonTapped(_ sender: Any) {
        let row = picker.selectedRow(inComponent: 0)
        let str = data[row]
        hideMainPickerView { [weak self] (true) in
            guard let this = self else {
                return
            }
            
            this.delegate?.dataPickerCotroller(controller: this, select: str)
            this.dismiss(animated: true, completion: nil)
        }
    }
    
}

// MARK: - UIPickerViewDataSource

extension DataPickerViewController: UIPickerViewDataSource {
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
}

// MARK: - UIPickerViewDelegate

extension DataPickerViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
}
