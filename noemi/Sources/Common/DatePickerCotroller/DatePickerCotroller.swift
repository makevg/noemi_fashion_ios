//
//  DatePickerCotroller.swift
//  noemi
//
//  Created by Maximychev Evgeny on 07.03.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol DatePickerCotrollerDelegate: class {
    func datePickerCotroller(controller: DatePickerCotroller, select date: Date)
}

class DatePickerCotroller: UIViewController {
    
    // MARK: - Public properties
    
    weak var delegate: DatePickerCotrollerDelegate?
    
    // MARK: - Private properties
    
    @IBOutlet fileprivate weak var mainPickerView: UIView!
    @IBOutlet fileprivate weak var datePicker: UIDatePicker!
    @IBOutlet fileprivate weak var mainPickerViewBottomConstraint: NSLayoutConstraint!
    
    fileprivate let animationDuration = 0.3
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.maximumDate = Date()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showMainPickerView()
    }
    
    // MARK: - Actions
    
    @IBAction func viewTapped(_ sender: Any) {
        close()
    }
    
    @IBAction func colseButtonTapped(_ sender: Any) {
        close()
    }
    

    @IBAction func selectDateButtonTapped(_ sender: Any) {
        let date = datePicker.date
        hideMainPickerView { [weak self] (true) in
            guard let this = self else {
                return
            }
            
            this.delegate?.datePickerCotroller(controller: this, select: date)
            this.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: - Extensions

// MARK: - Helpers

extension DatePickerCotroller {
    
    fileprivate func showMainPickerView() {
        UIView.animate(withDuration: animationDuration) { [weak self] in
            guard let this = self else {
                return
            }
            
            this.mainPickerViewBottomConstraint.constant = 0
            this.view.layoutIfNeeded()
        }
    }
    
    fileprivate func hideMainPickerView(completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: animationDuration,
                       animations: { [weak self] in
                        guard let this = self else {
                            return
                        }
                        
                        let mainPickerViewHeight = this.mainPickerView.frame.height
                        this.mainPickerViewBottomConstraint.constant = -mainPickerViewHeight
                        this.view.layoutIfNeeded()
            },
                       completion: completion)
    }
    
    fileprivate func close() {
        hideMainPickerView { [weak self] (true) in
            guard let this = self else {
                return
            }
            
            this.dismiss(animated: true, completion: nil)
        }
    }
    
}
