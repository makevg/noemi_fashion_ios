//
//  UIStoryboard+DatePicker.swift
//  noemi
//
//  Created by Maximychev Evgeny on 07.03.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    struct DatePicker {}
    
}

extension UIStoryboard.DatePicker {
    
    private static var datePickerStoryboard: UIStoryboard {
        return UIStoryboard.init(name: "DatePicker", bundle: nil)
    }
    
    static var datePickerController: DatePickerCotroller {
        return datePickerStoryboard.instantiateInitialViewController() as! DatePickerCotroller
    }
    
    static var dataPickerController: DataPickerViewController {
        let identifier = String(describing: DataPickerViewController.self)
        return datePickerStoryboard.instantiateViewController(withIdentifier: identifier) as! DataPickerViewController
    }
    
}
