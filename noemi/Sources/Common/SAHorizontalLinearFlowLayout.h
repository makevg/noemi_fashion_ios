//
//  SAHorizontalLinearFlowLayout.h
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SAHorizontalLinearFlowLayout : UICollectionViewFlowLayout

@property(assign, nonatomic) CGFloat scalingOffset;
@property(assign, nonatomic) CGFloat minimumScaleFactor;
@property(assign, nonatomic) BOOL scaleItems;

+ (SAHorizontalLinearFlowLayout *)layoutConfiguredWithCollectionView:(UICollectionView *)collectionView
                                                            itemSize:(CGSize)itemSize
                                                  minimumLineSpacing:(CGFloat)minimumLineSpacing;

- (CGFloat)pageWidth;

@end
