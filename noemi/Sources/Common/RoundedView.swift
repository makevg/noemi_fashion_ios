//
//  RoundedView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    // MARK: - Properties
    
    @IBInspectable var cornerRadius: CGFloat = 25
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = cornerRadius
    }

}
