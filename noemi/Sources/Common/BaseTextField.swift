//
//  BaseTextField.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class BaseTextField: UITextField {
    
    // MARK: - Properties
    
    fileprivate var accessoryView: InputAccessoryView {
        let rect = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 45)
        let view = InputAccessoryView(frame: rect)
        view.delegate = self
        return view
    }
    
    // MARK: - Override
    
    override func awakeFromNib() {
        inputAccessoryView = accessoryView
    }

}

// MARK: - InputAccessoryViewDelegate

extension BaseTextField: InputAccessoryViewDelegate {
    
    func inputAccessoryViewDidTapDoneButton(view: InputAccessoryView) {
        resignFirstResponder()
    }
    
}
