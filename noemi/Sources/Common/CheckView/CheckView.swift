//
//  CheckView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 04.07.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol CheckViewDelegate: class {
    func checkViewStateChanged(_ cheked: Bool)
}

class CheckView: UIView {

    // MARK: - Private properties
    
    @IBOutlet private var mainView: UIView!
    @IBOutlet private weak var okImageView: UIImageView!
    
    weak var delegate: CheckViewDelegate?
    
    var isCheked = true
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - UI Actions
    
    @IBAction func checkViewTapped(_ sender: Any) {
        isCheked = !isCheked
        configure(with: isCheked)
        
        delegate?.checkViewStateChanged(isCheked)
    }
    
    // MARK: - Private
    
    private func commonInit() {
        let key = String(describing: CheckView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
        
        configure(with: isCheked)
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }
    
    private func configure(with state: Bool) {
        let imageName = state ? "ok" : ""
        okImageView.image = UIImage(named: imageName)
    }
    
}
