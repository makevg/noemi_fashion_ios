//
//  EmptyView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    // MARK: - Properties
    
    static var emptyView: EmptyView {
        let view = EmptyView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        return view
    }
    
    var emptyText: String? {
        set { emptyTextLabel.text = newValue }
        get { return emptyTextLabel.text }
    }
    
    @IBOutlet fileprivate var mainView: UIView!
    @IBOutlet fileprivate weak var emptyTextLabel: UILabel!
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension EmptyView {
    
    fileprivate func commonInit() {
        let key = String(describing: EmptyView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }

}
