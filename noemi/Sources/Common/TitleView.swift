//
//  TitleView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 11.07.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol TitleViewDelegate: class {
    func titleViewTapped()
}

class TitleView: UIView {
    
    // MARK: - Private properties
    
    @IBOutlet private var mainView: UIView!
    
    weak var delegate: TitleViewDelegate?
    
    static var titleView: TitleView {
        let view = TitleView(frame: CGRect(x: 0, y: 0, width: 150, height: 50))
        return view
    }
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - UI Actions
    
    @IBAction func titleViewTapped(_ sender: Any) {
        delegate?.titleViewTapped()
    }
    
    // MARK: - Private
    
    private func commonInit() {
        let key = String(describing: TitleView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }

}
