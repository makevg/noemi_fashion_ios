//
//  ImageViewerController.swift
//  noemi
//
//  Created by Максимычев Е.О. on 17/10/2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ImageViewerController: UIViewController {
	
	// MARK: - Properties
	
	@IBOutlet fileprivate weak var scrollView: UIScrollView!
	@IBOutlet fileprivate weak var imageView: UIImageView!
	
	private var image: UIImage?
	
	// MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		scrollView.delegate = self
		imageView.image = image
	}
	
	// MARK: - Internal
	
	func configure(with image: UIImage) {
		self.image = image
	}
	
	// MARK: - Actions
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
}

// MARK: - UIScrollViewDelegate

extension ImageViewerController: UIScrollViewDelegate {
	
	func viewForZooming(in scrollView: UIScrollView) -> UIView? {
		return imageView
	}
	
}
