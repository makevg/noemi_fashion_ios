//
//  UIStoryboard+ImageViewer.swift
//  noemi
//
//  Created by Максимычев Е.О. on 17/10/2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

extension UIStoryboard {
	
	struct ImageViewer {}
	
}

extension UIStoryboard.ImageViewer {
	
	private static var imageViewerStoryboard: UIStoryboard {
		return UIStoryboard.init(name: "ImageViewer", bundle: nil)
	}
	
	static var imageViewerController: ImageViewerController {
		return imageViewerStoryboard.instantiateInitialViewController() as! ImageViewerController
	}
	
}
