//
//  LocationManagerProvider.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerProvider {
    var locationManager: CLLocationManager { get }
}

extension LocationManagerProvider {
    
    var locationManager: CLLocationManager {
        guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
            fatalError("Wrong delegate type")
        }
        
        return delegate.locationManager
    }
    
}
