//
//  ShadowButton.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ShadowButton: UIButton {
    
    @IBInspectable var shadowColor: UIColor = .black
    
    // MARK - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        localConfigure()
    }
    
    // MARK - Private
    
    private func localConfigure() {
        layer.cornerRadius = 25
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = 3.5
        layer.shadowOpacity = 0.7
    }

}
