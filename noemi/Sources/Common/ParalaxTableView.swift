//
//  ParalaxTableView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 2.09.2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

class ParalaxTableView: UITableView {

    var heightConstraint: NSLayoutConstraint?
    var bottomConstraint: NSLayoutConstraint?
	
	private var imageView: UIImageView? {
		guard let header = tableHeaderView,
			let imageView = header.subviews.first as? UIImageView else {
				return nil
		}
		
		return imageView
	}
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let header = tableHeaderView else { return }
        if heightConstraint == nil {
            if let imageView = header.subviews.first as? UIImageView {
                heightConstraint = imageView.constraints.filter{ $0.identifier == "height" }.first
                bottomConstraint = header.constraints.filter{ $0.identifier == "bottom" }.first
            }
        }
        
        /// contentOffset - is a dynamic value from the top
        /// adjustedContentInset fixed valie from the top
        let offsetY = -contentOffset.y// + adjustedContentInset.top)
        
        heightConstraint?.constant = max(header.bounds.height, header.bounds.height + offsetY)
        bottomConstraint?.constant = offsetY >= 0 ? 0 : offsetY / 2

        header.clipsToBounds = offsetY <= 0
    }
	
	func configure(with imageUrl: String) {
		imageView?.kf.setImage(with: URL(string: imageUrl),
													 placeholder: UIImage(named: "look_placeholder"),
													 progressBlock: nil,
													 completionHandler: nil)
	}
	
}
