//
//  Float+String.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

extension Float {
    
    func oneDigitAfterPointString() -> String {
        // TODO: Remove with right format
        return String(format: "%.1f", arguments: [self])
    }
    
}
