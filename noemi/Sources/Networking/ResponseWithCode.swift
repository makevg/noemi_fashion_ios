//
//  ResponseWithCode.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseWithCode<T: PaginatableType>: ImmutableMappable {
    
    public let objects: [T]
    public let code: ServiceCode
    
    public init(map: Map) throws {
        objects = try map.value(T.objectsKey)
        code = ServiceCode(rawValue: try map.value("status"))!
        
    }
    
}
