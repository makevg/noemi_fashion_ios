//
//  AuthorizationManager.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//


import Foundation
import KeychainAccess

public final class AuthorizationManager {
    
    public static let shared = AuthorizationManager()
    
    private var keychain = Keychain(service: "com.fashion.noemi")
    
    var accessToken: String {
        
        get {
            return keychain["authToken"] ?? ""
        }
        
        set(newToken) {
            keychain["authToken"] = newToken
        }
        
    }
    
    var password: String {
        
        get {
            return keychain["password"] ?? ""
        }
        
        set(newPassword) {
            keychain["password"] = newPassword
        }
        
    }
    
    public func clearAccessToken() {
        keychain["authToken"] = nil
    }
    
    
    
}
