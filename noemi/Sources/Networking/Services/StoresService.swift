//
//  StoresService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 16.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

typealias StoresCompletion = ([Store]?, ServiceCode?, ServiceError?) -> ()

final class StoresService: BasicService {
    
    func getStores(with completion: @escaping StoresCompletion) {
        NoemiProvider.request(.getStores) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<Store>.self)
                    completion(result.objects, result.code, nil)
                    
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func searchStores(with name: String, completion: @escaping StoresCompletion) {
        NoemiProvider.request(.searchStores(name: name)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<Store>.self)
                    completion(result.objects, result.code, nil)
                    
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }

        }
    }
    
}
