//
//  BasicService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper
import ObjectMapper

typealias BaseServiceCompletion = (ResponseInfo?, ServiceError?) -> ()

protocol BasicService {
    
}

extension BasicService {
    
    func fetchObject<S: TargetType, T: ImmutableMappable>(with provider: MoyaProvider<S>, target: S,
                     completion: @escaping ( (T?, ServiceError?) -> () ) ) {
        
        provider.request(target) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let object = try response.mapObject(T.self)
                    
                    completion(object, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
        
    }
    
    func fetchArray<S: TargetType, T: ImmutableMappable>(with provider: MoyaProvider<S>, target: S,
                    completion: @escaping ( ([T]?, ServiceError?) -> () ) ) {
        
        provider.request(target) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    let objects = try response.mapArray(T.self)
                    
                    completion(objects, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
        
    }
    
}
