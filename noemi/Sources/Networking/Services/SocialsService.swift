//
//  SocialsService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 08.11.2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

typealias SocialsCompletion = (Region?, ServiceCode?, ServiceError?) -> ()
typealias RegionsCompletion = ([Region]?, ServiceCode?, ServiceError?) -> ()

final class SocialsService: BasicService {
	
	func searchSocials(by coordinates: String, completion: @escaping SocialsCompletion) {
		NoemiProvider.request(.searchSocials(coordinates: coordinates)) { response in
			switch response {
			case let .success(response):
				do {
					let json = try response.mapJSON()
					print(json)
					
					guard let dictionary = json as? [String: Any],
						let code = dictionary["status"] as? String,
						let result = dictionary["result"] as? [String: Any] else {
							completion(nil, nil, nil)
							return
					}
					
					let region = Region(dictionary: result)
					let serviceCode = ServiceCode(rawValue: code)
					completion(region, serviceCode, nil)
				} catch {
					print(error)
					
					completion(nil, nil, .jsonMapping(response))
				}
				
			case let .failure(error):
				print(error)
				
				completion(nil, nil, error)
			}
		}
	}
	
	func getRegions(with completion: @escaping RegionsCompletion) {
		NoemiProvider.request(.getRegions) { response in
			switch response {
			case let .success(response):
				do {
					let json = try response.mapJSON()
					print(json)
					
					let result = try response.mapObject(ResponseWithCode<Region>.self)
					completion(result.objects, result.code, nil)
				} catch {
					print(error)
					
					completion(nil, nil, .jsonMapping(response))
				}
				
			case let .failure(error):
				print(error)
				
				completion(nil, nil, error)
			}
		}
	}
	
}
