//
//  LooksService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

typealias LooksCompletion = ([LookItem]?, ServiceCode?, ServiceError?) -> ()
typealias LookBookCompletion = ([LookBookItem]?, ServiceCode?, ServiceError?) -> ()
typealias LookBookCategoriesCompletion = ([LookBookCategory]?, ServiceCode?, ServiceError?) -> ()

final class LooksService: BasicService {
    
    func getProduct(with code: String, completion: @escaping LooksCompletion) {
        NoemiProvider.request(.getProduct(code: code)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<LookItem>.self)
                    completion(result.objects, result.code, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func setFavoriteProduct(with parameters: ParametrizableType, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.setFavoriteProduct(parameters: parameters)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
    func getPurchasesList(with completion: @escaping LooksCompletion) {
        NoemiProvider.request(.getPurchasesList) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<LookItem>.self)
                    completion(result.objects, result.code, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func getFavoritesList(with completion: @escaping LooksCompletion) {
        NoemiProvider.request(.getFavoritesList) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<LookItem>.self)
                    completion(result.objects, result.code, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
	func getLookBookList(with category: String?, completion: @escaping LookBookCompletion) {
        NoemiProvider.request(.getLookbookList(category: category ?? "")) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    let result = try response.mapObject(ResponseWithCode<LookBookItem>.self)
                    completion(result.objects, result.code, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
	
	func getLookBookCategories(with completion: @escaping LookBookCategoriesCompletion) {
		NoemiProvider.request(.getLookBookCategories) { response in
			switch response {
			case let .success(response):
				do {
					let json = try response.mapJSON()
					print(json)
					
					let result = try response.mapObject(ResponseWithCode<LookBookCategory>.self)
					completion(result.objects, result.code, nil)
				} catch {
					print(error)
					
					completion(nil, nil, .jsonMapping(response))
				}
				
			case let .failure(error):
				print(error)
				
				completion(nil, nil, error)
			}
		}
	}
    
}
