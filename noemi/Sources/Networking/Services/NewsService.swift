//
//  NewsService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.09.2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

typealias NewsListCompletion = ([News]?, ServiceCode?, ServiceError?) -> ()
typealias NewsCompletion = (News?, ServiceCode?, ServiceError?) -> ()

final class NewsService: BasicService {
	
	func getNewsList(with completion: @escaping NewsListCompletion) {
		NoemiProvider.request(.getNewsList) { response in
			switch response {
			case let .success(response):
				do {
					let json = try response.mapJSON()
					print(json)
					
					let result = try response.mapObject(ResponseWithCode<News>.self)
					completion(result.objects, result.code, nil)
				} catch {
					print(error)
					
					completion(nil, nil, .jsonMapping(response))
				}
				
			case let .failure(error):
				print(error)
				
				completion(nil, nil, error)
			}
		}
	}
	
	func getNews(with newsId: Int, completion: @escaping NewsCompletion) {
		NoemiProvider.request(.getNews(newsId: newsId)) { response in
			switch response {
			case let .success(response):
				do {
					let json = try response.mapJSON()
					print(json)
					
					guard let dictionary = json as? [String: Any],
						let code = dictionary["status"] as? String else {
							completion(nil, nil, nil)
							return
					}
					
					let result = try response.mapObject(ResponseWithCode<News>.self).objects.first
					let serviceCode = ServiceCode(rawValue: code)
					completion(result, serviceCode, nil)
				} catch {
					print(error)
					
					completion(nil, nil, .jsonMapping(response))
				}
				
			case let .failure(error):
				print(error)
				
				completion(nil, nil, error)
			}
		}
	}
	
}
