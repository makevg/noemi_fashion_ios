//
//  ProfileService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

typealias ProfileCompletion = (Profile?, ServiceCode?, ServiceError?) -> ()
typealias GetSaleCodeCompletion = (Int?, ServiceCode?, ServiceError?) -> ()

final class ProfileService: BasicService {

    func getProfileInfo(with completion: @escaping ProfileCompletion) {
        NoemiProvider.request(.getUserInfo) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any],
                        let code = dictionary["status"] as? String else {
                            completion(nil, nil, nil)
                            return
                    }
                    
                    let result = try response.mapObject(Profile.self)
                    let serviceCode = ServiceCode(rawValue: code)
                    completion(result, serviceCode, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func setProfileInfo(with parameters: ParametrizableType, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.setUserInfo(parameters: parameters)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
    func getUserSaleCode(with completion: @escaping GetSaleCodeCompletion) {
        NoemiProvider.request(.getCode) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any],
                        let saleCode = dictionary["code"] as? Int,
                        let code = dictionary["status"] as? String else {
                            completion(nil, nil, nil)
                            return
                    }
                    
                    let serviceCode = ServiceCode(rawValue: code)
                    completion(saleCode, serviceCode, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func getSaleCode(with completion: @escaping GetSaleCodeCompletion) {
        NoemiProvider.request(.getSaleCode) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any],
                        let saleCode = dictionary["code"] as? Int,
                        let code = dictionary["status"] as? String else {
                            completion(nil, nil, nil)
                            return
                    }
                    
                    let serviceCode = ServiceCode(rawValue: code)
                    completion(saleCode, serviceCode, nil)
                } catch {
                    print(error)
                    
                    completion(nil, nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, nil, error)
            }
        }
    }
    
    func getFriendSale(with code: String, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.getSaleByCode(code: code)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
    func sendPoints(with parameters: ParametrizableType, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.sendPoints(parameters: parameters)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
}
