//
//  AuthorizationService.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

final class AuthorizationService: BasicService {
    
    func userAuthorized() -> Bool {
        return AuthorizationManager.shared.accessToken != ""
    }
    
    func register(with phone: String, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.register(phone: phone)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                    
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
    func sendCode(with parameters: RegistrationContainer, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.sendCode(parameters: parameters)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    if let appToken = dictionary["hash"] as? String {
                        AuthorizationManager.shared.accessToken = appToken
                    }
                    
                    AuthorizationManager.shared.password = parameters.number
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                    
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
    func auth(with parameters: AuthContainer, completion: @escaping BaseServiceCompletion) {
        NoemiProvider.request(.auth(parameters: parameters)) { response in
            switch response {
            case let .success(response):
                do {
                    let json = try response.mapJSON()
                    print(json)
                    
                    guard let dictionary = json as? [String: Any] else {
                            completion(nil, nil)
                            return
                    }
                    
                    if let appToken = dictionary["hash"] as? String {
                        AuthorizationManager.shared.accessToken = appToken
                    }
                    
                    AuthorizationManager.shared.password = parameters.password
                    let responseInfo = ResponseInfo(with: dictionary)
                    completion(responseInfo, nil)
                    
                } catch {
                    print(error)
                    
                    completion(nil, .jsonMapping(response))
                }
                
            case let .failure(error):
                print(error)
                
                completion(nil, error)
            }
        }
    }
    
}
