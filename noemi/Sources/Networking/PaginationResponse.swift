//
//  PaginationResponse.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

protocol PaginatableType: ImmutableMappable {
    
    static var objectsKey: String { get }
    
}

struct PaginationResponse<T: PaginatableType>: ImmutableMappable {
    
    public let objects: [T]
    public let maxPages: Int
    public let code: ServiceCode
    
    public init(map: Map) throws {
        objects = try map.value(T.objectsKey)
        maxPages = try map.value("max_pages")
        code = ServiceCode(rawValue: try map.value("status"))!
    }
    
}

