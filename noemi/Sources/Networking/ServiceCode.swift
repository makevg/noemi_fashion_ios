//
//  ServiceCode.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

public enum ServiceCode: String {
    case success
    case error
}
