//
//  ResponseInfo.swift
//  noemi
//
//  Created by Maximychev Evgeny on 02.07.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseInfo {
    let code: ServiceCode
    var message: String?
    
    init(with code: ServiceCode, message: String?) {
        self.code = code
        self.message = message
    }
    
    init(with dictionary: [String:Any]) {
        code = ServiceCode(rawValue: (dictionary["status"] as? String)!)!
        
        if let msg = dictionary["message"] as? String {
            message = msg
        }
    }
}
