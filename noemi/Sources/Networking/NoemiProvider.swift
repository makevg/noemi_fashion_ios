//
//  NoemiProvider.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya

fileprivate let networkActivityPlugin = NetworkActivityPlugin { change, _ -> () in
    switch change {
    case .began:
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    case .ended:
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

fileprivate let endpointClosure = { (target: NoemiService) -> Endpoint in
    let url = target.baseURL.appendingPathComponent(target.path).absoluteString
	let endpoint = Endpoint(url: url,
													sampleResponseClosure: {.networkResponse(200, target.sampleData)},
													method: target.method,
													task: target.task,
													httpHeaderFields: nil)
    let token = AuthorizationManager.shared.accessToken
    switch target {
    case .register, .sendCode, .auth, .getLookbookList, .getStores, .searchStores, .getProduct, .searchSocials:
        return endpoint
    default:
        return endpoint.adding(newHTTPHeaderFields: ["Autorization": token])
    }
}

fileprivate let stubClosure = { (target: NoemiService) -> Moya.StubBehavior in
    switch target {
    default:
        return .never
    }
}

// MARK: - Provider

let NoemiProvider = MoyaProvider<NoemiService>(endpointClosure: endpointClosure,
                                               stubClosure: stubClosure,
                                               plugins: [NetworkLoggerPlugin(verbose: true), networkActivityPlugin])

// MARK: - Target

enum NoemiService {
    case register(phone: String)
    case sendCode(parameters: ParametrizableType)
    case auth(parameters: ParametrizableType)
    case getUserInfo
    case setUserInfo(parameters: ParametrizableType)
    case getCode
    case sendPoints(parameters: ParametrizableType)
    case getStores
    case searchStores(name: String)
    case getProduct(code: String)
    case getFavoritesList
    case setFavoriteProduct(parameters: ParametrizableType)
    case getPurchasesList
    case getSaleCode
    case getSaleByCode(code: String)
    case getLookbookList(category: String)
	case getLookBookCategories
	case searchSocials(coordinates: String)
	case getRegions
	case getNewsList
	case getNews(newsId: Int)
	case subscribe(parameters: ParametrizableType)
}

// MARK: - TargetType Protocol Implementation

extension NoemiService: TargetType {
	var headers: [String : String]? {
		return ["Autorization": AuthorizationManager.shared.accessToken]
	}
	
    
    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var baseURL: URL { return URL(string: "https://node.noemifashion.com")! }
    
    var path: String {
        switch self {
        case .register:
            return "/usr/in"
        case .sendCode:
            return "/usr/in"
        case .auth:
            return "/usr/in"
        case .getUserInfo:
            return "/usr/get"
        case .setUserInfo:
            return "/usr/set"
        case .getCode:
            return "/usr/getCode"
        case .sendPoints:
            return "/usr/sendPoints"
        case .getStores:
            return "/score/search"
        case let .searchStores(name):
            return "/score/searchString/\(name)"
        case let .getProduct(code):
            return "/product/search/\(code)"
        case .getFavoritesList:
            return "/product/favorite/list"
        case .setFavoriteProduct:
            return "/product/favorite/edit"
        case .getPurchasesList:
            return "/product/my"
        case .getSaleCode:
            return "/code/get"
        case let .getSaleByCode(code):
            return "/code/set/\(code)"
        case let .getLookbookList(category):
            return "/lookbook/list/\(category)"
				case .getLookBookCategories:
					return "/lookbook/category"
				case let .searchSocials(coordinates):
					return "/social/search/\(coordinates)"
				case .getRegions:
					return "/social/regions"
				case .getNewsList:
					return "/news/list"
				case let .getNews(newsId):
					return "/news/\(newsId)"
				case .subscribe:
					return "/firebase/subscription"
			}
    }
    
    var method: Moya.Method {
        switch self {
        case .register:
            return .post
        case .sendCode:
            return .post
        case .auth:
            return .post
        case .sendPoints:
            return .post
        case .setUserInfo:
            return .post
        case .setFavoriteProduct:
            return .post
				case .subscribe:
					return .post
        default:
            return .get
        }
    }
    
    var task: Task {
			switch self {
			case let .register(phone):
				return .requestParameters(parameters: ["phone" : phone],
																	encoding: parameterEncoding)
			case let .sendCode(parameters),
					 let .auth(parameters),
					 let .sendPoints(parameters),
					 let .setUserInfo(parameters),
					 let .setFavoriteProduct(parameters),
					 let .subscribe(parameters):
				return .requestParameters(parameters: parameters.parametersForRequest(),
																	encoding: parameterEncoding)
			default:
				return .requestPlain
    }
	}
    
    var multipartBody: [MultipartFormData]? {
        switch self {
        default:
            return []
        }
    }
    
    var sampleData: Data {
        switch self {
            
        default:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        }
    }
    
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return self.data(using: .utf8)!
    }
}


// MARK: - Stubbed responses

class BundleDetection {}

func stubbedResponse(withFilename name: String) -> Data {
    do {
        guard let path = Bundle(for: BundleDetection.self).path(forResource: name, ofType: "json") else {
            return Data()
        }
        
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        return data
    }
    catch {
        return Data()
    }
}
