//
//  ServiceError.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import Moya

// Hide Moya error for upper layers
public typealias ServiceError = Moya.MoyaError
