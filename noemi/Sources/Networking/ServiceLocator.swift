//
//  ServiceLocator.swift
//  noemi
//
//  Created by Maximychev Evgeny on 13.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

final class ServiceLocator {
    
    static let shared = ServiceLocator()
    
    let authService: AuthorizationService = AuthorizationService()
    let profileSrevice: ProfileService = ProfileService()
    let storesService: StoresService = StoresService()
    let looksService: LooksService = LooksService()
	let socialsService: SocialsService = SocialsService()
	let newsService: NewsService = NewsService()
    
}
