//
//  LookItem.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct LookItem {
    
    let uuid: Int
    let name: String
    let category: String
    let imageUrl: String
    let images: [ImageItem]
    let info: String
    let size: String
    let discountPrice: String
    let price: String
    let isFavorite: Bool
    let isPurchased: Bool
    let purchaseDate: Double
    let favoriteDate: Double
    
}

extension LookItem: ImmutableMappable {
    
    public init(map: Map) throws {
        uuid = try map.value("id")
        name = try map.value("name")
        category = try map.value("category")
        imageUrl = try map.value("previewImage")
        images = try map.value("images")
        info = try map.value("info")
        size = try map.value("size")
        discountPrice = try map.value("discountPrice")
        price = try map.value("price")
        isFavorite = try map.value("isFavorite")
        isPurchased = try map.value("isPurchased")
        purchaseDate = try map.value("date")
        favoriteDate = try map.value("favoriteDate")
    }
    
}

extension LookItem: PaginatableType {
    
    static var objectsKey: String {
        return "result"
    }
    
}
