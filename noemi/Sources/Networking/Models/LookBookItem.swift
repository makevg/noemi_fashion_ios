//
//  LookBookItem.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct LookBookItem {
    
    let uuid: Int
    let imageUrl: String
    let width: Double
    let height: Double
    
}

extension LookBookItem: ImmutableMappable {
    
    public init(map: Map) throws {
        uuid = try map.value("id")
        imageUrl = try map.value("url")
        width = try map.value("width")
        height = try map.value("height")
    }
    
}

extension LookBookItem: PaginatableType {
    
    static var objectsKey: String {
        return "result"
    }
    
}
