//
//  ImageItem.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct ImageItem {
    
    let width: Int
    let height: Int
    let url: String
    
}

extension ImageItem: ImmutableMappable {
    
    public init(map: Map) throws {
        width = try map.value("width")
        height = try map.value("height")
        url = try map.value("url")
    }
    
}
