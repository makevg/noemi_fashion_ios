//
//  Profile.swift
//  noemi
//
//  Created by Maximychev Evgeny on 20.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct Profile {
	
	let name: String
	let surname: String
	let gender: String
	let birthday: String
	let discount: Int
	let hiddenPoints: Int
	let points: Int
	let card: String
	let phone: String
	let email: String
    
}

extension Profile: ImmutableMappable {
	
	public init(map: Map) throws {
		name = try map.value("name")
		surname = try map.value("surname")
		gender = try map.value("floor")
		birthday = try map.value("birthday")
		discount = try map.value("discount")
		hiddenPoints = try map.value("hiddenPoints")
		points = try map.value("points")
		card = try map.value("card")
		phone = try map.value("phone")
		email = try map.value("email")
    }
    
}
