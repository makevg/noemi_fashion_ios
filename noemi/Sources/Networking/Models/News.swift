//
//  News.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.09.2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct News {
	
	let uuid: Int
	let name: String
	let image: String
	let annotation: String
	let content: String
	let date: Double
	
}

extension News: ImmutableMappable {
	
	public init(map: Map) throws {
		uuid = try map.value("uid")
		name = try map.value("name")
		image = try map.value("image")
		annotation = try map.value("annotation")
		content = try map.value("content")
		date = try map.value("dtime")
	}
	
}

extension News: PaginatableType {
	
	static var objectsKey: String {
		return "result"
	}
	
}
