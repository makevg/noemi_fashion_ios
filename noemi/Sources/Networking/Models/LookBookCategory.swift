//
//  LookBookCategory.swift
//  noemi
//
//  Created by Максимычев Е.О. on 05/06/2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct LookBookCategory {
	
	let uuid: Int
	let name: String
	
}

extension LookBookCategory: ImmutableMappable {
	
	public init(map: Map) throws {
		uuid = try map.value("id")
		name = try map.value("name")
	}
	
}

extension LookBookCategory: PaginatableType {
	
	static var objectsKey: String {
		return "result"
	}
	
}
