//
//  Store.swift
//  noemi
//
//  Created by Maximychev Evgeny on 16.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct Store {
    
    let uuid: Int
    let name: String
    let city: String
    let latitude: String
    let longitude: String
    let address: String
    let email: String
    let phone: String
    let work: String
    
    init(uuid: Int,
         name: String,
         city: String,
         latitude: String,
         longitude: String,
         address: String,
         email: String,
         phone: String,
         work: String)
    {
        self.uuid = uuid
        self.name = name
        self.city = city
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
        self.email = email
        self.phone = phone
        self.work = work
    }
    
}

extension Store: ImmutableMappable {

    public init(map: Map) throws {
        uuid = try map.value("id")
        name = try map.value("name")
        city = try map.value("city")
        latitude = try map.value("latitude")
        longitude = try map.value("longitude")
        address = try map.value("address")
        email = try map.value("email")
        phone = try map.value("phone")
        work = try map.value("work")
    }
    
}

extension Store: PaginatableType {
    
    static var objectsKey: String {
        return "result"
    }
    
}
