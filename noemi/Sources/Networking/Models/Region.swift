//
//  Region.swift
//  noemi
//
//  Created by Maximychev Evgeny on 08.11.2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import ObjectMapper

struct Region {
	
	let uuid: Int
	let name: String
	let ok: String
	let vk: String
	let facebook: String
	let facebookId: String
	let instagram: String
	
	init(uuid: Int,
	     name: String,
	     ok: String,
	     vk: String,
	     facebook: String,
	     facebookId: String,
	     instagram: String)
	{
		self.uuid = uuid
		self.name = name
		self.ok = ok
		self.vk = vk
		self.facebook = facebook
		self.facebookId = facebookId
		self.instagram = instagram
	}
	
	init(dictionary: [String: Any]) {
		uuid = dictionary["id"] as! Int
		name = dictionary["name"] as! String
		ok = dictionary["ok"] as! String
		vk = dictionary["vk"] as! String
		facebook = dictionary["facebook"] as! String
		facebookId = dictionary["facebookId"] as! String
		instagram = dictionary["instagram"] as! String
	}
	
}

extension Region: ImmutableMappable {
	
	public init(map: Map) throws {
		uuid = try map.value("id")
		name = try map.value("name")
		ok = try map.value("ok")
		vk = try map.value("vk")
		facebook = try map.value("facebook")
		facebookId = try map.value("facebookId")
		instagram = try map.value("instagram")
	}
	
}

extension Region: PaginatableType {
	
	static var objectsKey: String {
		return "result"
	}
	
}
