//
//  UIStoryboard+Accessors.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

extension UIStoryboard {
	
	struct Main {}
	struct SignIn {}
	struct QRScanner {}
	struct Shops {}
	struct Looks {}
	struct News {}
    
}

extension UIStoryboard.Main {
    
    private static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    static var tabBarController: UITabBarController {
        return storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
    }
    
    static var socialSubscribeController: SocialSubscribeViewController {
        let identifier = String(describing: SocialSubscribeViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! SocialSubscribeViewController
    }
    
}

extension UIStoryboard.SignIn {
    
    private static var storyboard: UIStoryboard {
        return UIStoryboard(name: "SignIn", bundle: nil)
    }
    
    static var initialController: UIViewController {
        return storyboard.instantiateInitialViewController()!
    }
    
}

extension UIStoryboard.QRScanner {
	
	private static var storyboard: UIStoryboard {
		return UIStoryboard(name: "QRScanner", bundle: nil)
	}
	
	static var initialController: UIViewController {
		return storyboard.instantiateInitialViewController()!
	}
	
}

extension UIStoryboard.Shops {
    
    private static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Shops", bundle: nil)
    }
    
    static var shopsListViewController: ShopsListViewController {
        let identifier = String(describing: ShopsListViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! ShopsListViewController
    }
    
    static var mapViewController: MapViewController {
        let identifier = String(describing: MapViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! MapViewController
    }
    
}

extension UIStoryboard.Looks {
    
    private static var storyboard: UIStoryboard {
        return UIStoryboard(name: "Looks", bundle: nil)
    }
    
    static var createLookViewController: CreateLookViewController {
        let identifier = String(describing: CreateLookViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! CreateLookViewController
    }
    
    static var myLooksViewController: MyLooksViewController {
        let identifier = String(describing: MyLooksViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! MyLooksViewController
    }
    
    static var looksListViewController: LooksListViewController {
        let identifier = String(describing: LooksListViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! LooksListViewController
    }
    
    static var favoriteLooksViewController: FavoriteLooksViewController {
        let identifier = String(describing: FavoriteLooksViewController.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! FavoriteLooksViewController
    }
	
	
	static var looksBooksCategoriesViewController: LookBookCategoriesViewController {
		let identifier = String(describing: LookBookCategoriesViewController.self)
		return storyboard.instantiateViewController(withIdentifier: identifier) as! LookBookCategoriesViewController
	}
    
}

extension UIStoryboard.News {
	
	private static var storyboard: UIStoryboard {
		return UIStoryboard(name: "News", bundle: nil)
	}
	
	static var newsListViewController: NewsListVC {
		let identifier = String(describing: NewsListVC.self)
		return storyboard.instantiateViewController(withIdentifier: identifier) as! NewsListVC
	}
	
	static var newsCardViewController: NewsCardVC {
		let identifier = String(describing: NewsCardVC.self)
		return storyboard.instantiateViewController(withIdentifier: identifier) as! NewsCardVC
	}
	
}
