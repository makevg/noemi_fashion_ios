//
//  FavoriteLooksProvider.swift
//  noemi
//
//  Created by Maximychev Evgeny on 12.10.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import CoreData

final class FavoriteLooksProvider {
	
	static let shared = FavoriteLooksProvider()
	
	// MARK: - Internal
	
	func getFavoriteLooks() -> [FavoriteLook] {
		var looks = [FavoriteLook]()
		
		if #available(iOS 10.0, *) {
			do {
				looks = try persistentContainer.viewContext.fetch(FavoriteLook.fetchRequest())
			} catch {
				fatalError("ERROR: FavoriteLooks fetching failed")
			}
		} else {
			do {
				looks = try managedObjectContext.fetch(FavoriteLook.fetchRequest())
			} catch {
				fatalError("ERROR: FavoriteLooks fetching failed")
			}
		}
		
		return looks
	}
	
	func addLook(_ look: LookBookItem) {
		let isExist = lookIsExist(look).0
		
		if !isExist {
			if #available(iOS 10.0, *) {
				let context = persistentContainer.viewContext
				let favoriteLook = FavoriteLook(context: context)
				favoriteLook.uuid = Int32(look.uuid)
				favoriteLook.imageUrl = look.imageUrl
				favoriteLook.date = Date()
			} else {
				if let entityDesc = NSEntityDescription.entity(forEntityName: "FavoriteLook", in: managedObjectContext) {
					let favoriteLook = FavoriteLook(entity: entityDesc, insertInto: managedObjectContext)
					favoriteLook.uuid = Int32(look.uuid)
					favoriteLook.imageUrl = look.imageUrl
					favoriteLook.date = Date()
				}
			}
			
			saveContext()
		}
	}
	
	func removeLook(_ look: LookBookItem) {
		let result = lookIsExist(look)
		
		guard result.0, let findedLook = result.1 else { return }
		
		if #available(iOS 10.0, *) {
			let context = persistentContainer.viewContext
			context.delete(findedLook)
		} else {
			managedObjectContext.delete(findedLook)
		}
		
		saveContext()
	}
	
	func lookIsFavorite(_ look: LookBookItem) -> Bool {
		let isFavorite = lookIsExist(look).0
		return isFavorite
	}
	
	func lookIsFavorite(_ favoriteLook: FavoriteLook) -> Bool {
		let request = favoriteLooksFetchRequest
		request.predicate = NSCompoundPredicate(type: .and,
																						subpredicates: [NSPredicate(format: "uuid = %@", favoriteLook.uuid),
																														NSPredicate(format: "imageUrl = %@", favoriteLook.imageUrl ?? "")])
		var findedLook: FavoriteLook?
		
		if #available(iOS 10.0, *) {
			do {
				findedLook =  try persistentContainer.viewContext.fetch(request).first as? FavoriteLook
			} catch {
				fatalError("ERROR: FavoriteLooks fetching failed")
			}
		} else {
			do {
				findedLook = try managedObjectContext.fetch(request).first as? FavoriteLook
			} catch {
				fatalError("ERROR: FavoriteLooks fetching failed")
			}
		}
		
		return findedLook != nil
	}
	
	private func lookIsExist(_ look: LookBookItem) -> (Bool, FavoriteLook?) {
		var lookIsExist = false
		var fLook: FavoriteLook?
		
		for findedLook in getFavoriteLooks() {
			if findedLook.uuid == look.uuid && findedLook.imageUrl == look.imageUrl {
				lookIsExist = true
				fLook = findedLook
				break
			}
		}
		
		return (lookIsExist, fLook)
	}
	
	// MARK: - Core Data stack
	
	@available(iOS 10.0, *)
	lazy var persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: "FavoriteLooks")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error as NSError? {
				fatalError("Unresolved error \(error), \(error.userInfo)")
			}
		})
		return container
	}()
	
	var favoriteLooksFetchRequest: NSFetchRequest<NSFetchRequestResult> {
		if #available(iOS 10.0, *) {
			return FavoriteLook.fetchRequest()
		} else {
			return NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteLook")
		}
	}
	
	// MARK: - Core Data Saving support
	
	func saveContext () {
		if #available(iOS 10.0, *) {
			let context = persistentContainer.viewContext
			if context.hasChanges {
				do {
					try context.save()
				} catch {
					let nserror = error as NSError
					fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
				}
			}
		} else {
			if managedObjectContext.hasChanges {
				do {
					try managedObjectContext.save()
				} catch {
					let nserror = error as NSError
					NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
					abort()
				}
			}
		}
	}
	
	lazy var applicationDocumentsDirectory: URL = {
		let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		return urls.last!
	}()
	
	lazy var managedObjectModel: NSManagedObjectModel = {
		let modelURL = Bundle(for: FavoriteLooksProvider.self).url(forResource: "FavoriteLooks", withExtension: "momd")!
		return NSManagedObjectModel(contentsOf: modelURL)!
	}()
	
	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
		let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		let url = self.applicationDocumentsDirectory.appendingPathComponent("FavoriteLooks.sqlite")
		do {
			try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
		} catch let  error as NSError {
			fatalError("cannot setup persistent store coordinator")
		}
		return coordinator
	}()
	
	lazy var managedObjectContext: NSManagedObjectContext = {
		let coordinator = self.persistentStoreCoordinator
		var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		context.persistentStoreCoordinator = coordinator
		
		return context
	}()
	
	lazy var backgroundManagedObjectContext: NSManagedObjectContext = {
		let coordinator = self.persistentStoreCoordinator
		var context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		context.persistentStoreCoordinator = coordinator
		
		return context
	}()
	
	init() {
		let center = NotificationCenter.default
		center.addObserver(self, selector: #selector(managedObjectContextDidSave),
											 name: .NSManagedObjectContextDidSave, object: managedObjectContext)
		center.addObserver(self, selector: #selector(backgroundManagedObjectContextDidSave),
											 name: .NSManagedObjectContextDidSave, object: backgroundManagedObjectContext)
	}
	
	@objc func managedObjectContextDidSave(notification: Notification) {
		let context = backgroundManagedObjectContext
		context.perform {
			context.mergeChanges(fromContextDidSave: notification)
		}
	}
	
	@objc func backgroundManagedObjectContextDidSave(notification: Notification) {
		let context = managedObjectContext
		context.perform {
			context.mergeChanges(fromContextDidSave: notification)
		}
	}
    
}
