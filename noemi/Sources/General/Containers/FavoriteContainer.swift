//
//  FavoriteContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

struct FavoriteContainer {
    
    let productId: Int
    let status: Bool
    
}

extension FavoriteContainer: ParametrizableType {
    
    func parametersForRequest() -> [String : Any] {
        return ["id" : productId,
                "number" : status]
    }
    
}
