//
//  ProfileContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 20.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

struct ProfileContainer {
    
    let name: String
    let surname: String
    let birthday: String
    let gender: Int
    let password: String
    let phone: String
    let email: String
    let card: String
    
}

extension ProfileContainer: ParametrizableType {
    
    func parametersForRequest() -> [String : Any] {
        return ["name" : name,
                "surname" : surname,
                "birthday" : birthday,
                "floor" : gender,
                "password" : password,
                "phone" : phone,
                "email" : email,
                "card" : card]
    }
    
}
