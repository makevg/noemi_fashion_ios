//
//  SendPointsContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

struct SendPointsContainer {
    
    let phone: String
    let points: Int
    
}

extension SendPointsContainer: ParametrizableType {
    
    func parametersForRequest() -> [String : Any] {
        return ["phone" : phone,
                "points" : points]
    }
    
}
