//
//  SubscribeContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 04/01/2019.
//  Copyright © 2019 Maximychev Evgeny. All rights reserved.
//

import Foundation

//ParametrizableType

struct SubscribeContainer {
	let deviceToken: String
}

extension SubscribeContainer: ParametrizableType {
	
	func parametersForRequest() -> [String : Any] {
		return ["deviceId" : deviceToken]
	}
	
}
