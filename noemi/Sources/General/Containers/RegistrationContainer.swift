//
//  RegistrationContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 15.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

struct RegistrationContainer {

    let phone: String
    let number: String
    
}

extension RegistrationContainer: ParametrizableType {
    
    func parametersForRequest() -> [String : Any] {
        return ["phone" : phone,
                "number" : number]
    }
    
}
