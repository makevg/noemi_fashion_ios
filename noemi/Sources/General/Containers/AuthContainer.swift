//
//  AuthContainer.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

struct AuthContainer {
    
    let login: String
    let password: String
    
}

extension AuthContainer: ParametrizableType {
    
    func parametersForRequest() -> [String : Any] {
        return ["login" : login,
                "password" : password]
    }
    
}
