//
//  RateUsManager.swift
//  noemi
//
//  Created by Максимычев Е.О. on 29/12/2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import StoreKit

final class RateUsManager {
	
	// MARK: - Static
	
	static func checkAndAskForReview() {
		if let nextRateUsShownDate = Settings.shared.nextRateUsShownDate {
			if Date() >= nextRateUsShownDate {
				RateUsManager().requestReview()
			}
		} else {
			RateUsManager().requestReview()
		}
	}
	
	// MARK: - Private
	
	private func requestReview() {
		if #available(iOS 10.3, *) {
			SKStoreReviewController.requestReview()
		} else {
			let alert = UIAlertController(title: "Оцените наше приложение", message: "", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "Оставить", style: .default, handler: { _ in
				UrlOpener.open("itms-apps://itunes.apple.com/app/1253556820")
			}))
			alert.addAction(UIAlertAction(title: "Позже", style: .cancel, handler: nil))
			
			UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
		}
		
		Settings.shared.nextRateUsShownDate = Date().addingTimeInterval(24 * 60 * 60)
	}
	
}
