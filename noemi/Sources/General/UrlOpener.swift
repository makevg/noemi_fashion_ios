//
//  UrlOpener.swift
//  noemi
//
//  Created by Максимычев Е.О. on 29/12/2017.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

final class UrlOpener {
	
	static func open(_ urlStr: String) {
		guard let url = URL(string: urlStr) else { return }
		
		let app = UIApplication.shared
		if app.canOpenURL(url) {
			if #available(iOS 10.0, *) {
				app.open(url, options: [:], completionHandler: nil)
			} else {
				app.openURL(url)
			}
		}
	}
	
}
