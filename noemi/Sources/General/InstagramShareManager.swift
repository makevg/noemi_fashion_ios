//
//  InstagramShareManager.swift
//  noemi
//
//  Created by Maximychev Evgeny on 29.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Foundation

class InstagramShareManager: NSObject, UIDocumentInteractionControllerDelegate {
    
    // MARK: - Properties
    
    private let instagramURL = "instagram://app"
    private let uti = "com.instagram.exclusivegram"
    private let fileNameExtension = "instagram.igo"
    private let documentInteractionController = UIDocumentInteractionController()
    
    static let shared = InstagramShareManager()
    
    // MARK: - Public
    
    func instagramAppIsAvailable() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string: instagramURL)!)
    }
    
    func postImage(_ image: UIImage, caption: String, view: UIView, presentingController: UIViewController) {
        if instagramAppIsAvailable() {
					let jpgPath = (NSTemporaryDirectory() as NSString).appendingPathComponent(fileNameExtension)
					let imageData = image.jpegData(compressionQuality: 1)!
					try? imageData.write(to: URL(string: jpgPath)!)
            let rect = CGRect(x: 0, y: 0, width: 612, height: 612)
            let fileURL = NSURL.fileURL(withPath: jpgPath)
            documentInteractionController.url = fileURL
            documentInteractionController.delegate = self
            documentInteractionController.uti = uti
            
            documentInteractionController.annotation = ["InstagramCaption": caption]
            documentInteractionController.presentOpenInMenu(from: rect, in: view, animated: true)
        } else {
            let alert = UIAlertController(title: "Error", message: "Please install the Instagram application", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        }
    }
    
}
