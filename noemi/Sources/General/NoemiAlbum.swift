//
//  NoemiAlbum.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Photos

typealias ObtainNoemiAssetsCompletion = ([PHAsset]?) -> ()

class NoemiAlbum {
    
    // MARK: - Properties
    
    static let shared = NoemiAlbum()
    fileprivate let albumTitle = "noemi"
    
    // MARK: - Public
    
    func saveImage(_ image: UIImage) {
        obtainAlbum { (collection) in
            guard let album = collection else { return }
            
            PHPhotoLibrary.shared().performChanges({
                let assetRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
                let albumChangeRequest = PHAssetCollectionChangeRequest(for: album)
                if let assetPlaceholder = assetRequest.placeholderForCreatedAsset {
                    let assetPlaceholders: NSArray = [assetPlaceholder]
                    albumChangeRequest?.addAssets(assetPlaceholders)
                }
            }, completionHandler: nil)
        }
    }
    
    func obtainAssets(with completion: @escaping ObtainNoemiAssetsCompletion) {
        obtainAlbum { (collection) in
            guard let album = collection else {
                completion(nil)
                return
            }
            
            let assetsOptions = PHFetchOptions()
            assetsOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            let result = PHAsset.fetchAssets(in: album, options: assetsOptions)
            
            var assets = [PHAsset]()
            result.enumerateObjects({ (asset, _, _) in
                assets.append(asset)
            })
            
            completion(assets)
        }
        
        
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension NoemiAlbum {
    
    fileprivate func obtainAlbum(completion: @escaping (PHAssetCollection?) -> () ) {
        if let album = fetchAlbum() {
            completion(album)
        }
        else {
            PHPhotoLibrary.shared().performChanges({
                PHAssetCollectionChangeRequest.creationRequestForAssetCollection(withTitle: self.albumTitle)
            }, completionHandler: { [weak self] (succes, error) in
                guard let this = self,
                    succes == true,
                    let album = this.fetchAlbum() else
                {
                    completion(nil)
                    return
                }
                
                completion(album)
            })
        }
    }
    
    private func fetchAlbum() -> PHAssetCollection? {
        let options = PHFetchOptions()
        options.predicate = NSPredicate(format: "title = %@", albumTitle)
        let collection = PHAssetCollection.fetchAssetCollections(with: .album,
                                                                 subtype: .any,
                                                                 options: options)
        guard let album = collection.firstObject else { return nil }
        return album
    }
    
}
