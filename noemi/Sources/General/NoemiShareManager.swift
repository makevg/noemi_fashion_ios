//
//  NoemiShareManager.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class NoemiShareManager {
    
    // MARK: - Properties
    
    static let shared = NoemiShareManager()
    
    fileprivate let instagramShareManager = InstagramShareManager()
    fileprivate let service = ServiceLocator.shared.profileSrevice
    
    // MARK: - Public
    
    func showShareActionSheet(with lookItem: LookItem, image: UIImage, in controller: UIViewController) {
        var text = ""
        if !lookItem.name.isEmpty {
            text.append("Наименование: \(lookItem.name)\n")
        }
        if !lookItem.size.isEmpty {
            text.append("Размер: \(lookItem.size)\n")
        }
        if !lookItem.price.isEmpty {
            text.append("Цена: \(lookItem.price) руб\n")
        }
        
        showShareActionSheet(with: text, image: image, in: controller)
    }
    
    func showShareActionSheet(with image: UIImage, in controller: UIViewController) {
        showShareActionSheet(with: "", image: image, in: controller)
    }
    
    func showActivityController(with items: [Any], in controller: UIViewController) {
        let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceView = controller.view
            popoverController.sourceRect = controller.view.bounds
        }
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension NoemiShareManager {
    
    fileprivate func showShareActionSheet(with text: String, image: UIImage, in controller: UIViewController) {
        if AuthorizationManager.shared.accessToken == "" {
            showActionSheet(with: text, image: image, controller: controller)
            return
        }
        
        service.getSaleCode { [weak self] (saleCode, code, error) in
            guard let this = self,
                let saleCode = saleCode,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: controller)
                return
            }
            
            switch serviceCode {
            case .success:
                let codeStr = "\n\(saleCode)\nАктивируй код в приложении noemi и получи скидку в магазине. Подробнее на noemifashion.com\n"
                let saleCodeText: String = text + codeStr
                let imageWithText = image.addText(saleCodeText)
                this.showActionSheet(with: saleCodeText, image: imageWithText, controller: controller)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: controller)
            }
        }
    }
    
    fileprivate func showActivityController(with text: String, image: UIImage, in controller: UIViewController) {
        let items = [text, image] as [Any]
        showActivityController(with: items, in: controller)
    }
    
    private func showActionSheet(with text: String, image: UIImage, controller: UIViewController) {
        let actionSheet = UIAlertController(title: "", message: "Поделиться через...", preferredStyle: .actionSheet)
        
        if instagramShareManager.instagramAppIsAvailable() {
            actionSheet.addAction(UIAlertAction(title: "Instagram", style: .default, handler: { [weak self] (_) in
                guard let this = self else { return }
                this.instagramShareManager.postImage(image, caption: text, view: controller.view, presentingController: controller)
            }))
        }
        
        actionSheet.addAction(UIAlertAction(title: "Остальное", style: .default, handler: { [weak self] (_) in
            guard let this = self else { return }
            this.showActivityController(with: text, image: image, in: controller)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = controller.view
            popoverController.sourceRect = controller.view.bounds
        }
        controller.present(actionSheet, animated: true, completion: nil)
    }
    
}
