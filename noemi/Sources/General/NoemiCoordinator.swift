//
//  NoemiCoordinator.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class NoemiCoordinator {
    
    // MARK: - Properties
    
    fileprivate var userAuthorized: Bool {
        let tokenIsEmpty = AuthorizationManager.shared.accessToken.isEmpty
        return !tokenIsEmpty
    }
    
    
    
}
