//
//  Settings.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class Settings {
	
	static let shared = Settings()
	
	fileprivate let defaults = UserDefaults.standard
	
	var isFirstLaunch: Bool {
		
		get {
			guard let returnValue = defaults.object(forKey: "isFirstLaunchKey") as? Bool else {
				return true
			}
			
			return returnValue
		}
		
		set {
			defaults.set(newValue, forKey: "isFirstLaunchKey")
		}
	}
	
	var nextRateUsShownDate: Date? {
		
		get {
			guard let returnValue = defaults.object(forKey: "nextRateUsShownDateKey") as? Date else {
				return nil
			}
			
			return returnValue
		}
		
		set {
			defaults.set(newValue, forKey: "nextRateUsShownDateKey")
		}
		
	}
	
	func clearAccessToken() {
		AuthorizationManager.shared.clearAccessToken()
	}
    
}
