//
//  SignInViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var phoneField: BaseTextField!
    @IBOutlet fileprivate weak var passwordField: BaseTextField!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

// MARK: - UI Actions

extension SignInViewController {
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        guard let phone = phoneField.text, let password = passwordField.text else {
            return
        }
        
        let login = "+7" + phone
        let container = AuthContainer(login: login, password: password)
        let service = ServiceLocator.shared.authService
        service.auth(with: container) { [weak self] (info, error) in
            guard let this = self else { return }
            
            guard let responseInfo = info, error == nil else {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch responseInfo.code {
            case .success:
                this.updateFunctionalMode()
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this)
            }
        }
    }
    
}
