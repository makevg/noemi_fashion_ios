//
//  SendCodeViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 17.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class SendCodeViewController: BaseViewController {
    
    // MARK: - Properties
    
    var phone: String?
    
    @IBOutlet fileprivate weak var descrLabel: UILabel!
    @IBOutlet fileprivate weak var codeField: UITextField!
    
    @IBOutlet fileprivate weak var firstLine: UIView!
    @IBOutlet fileprivate weak var firstLabel: UILabel!
    @IBOutlet fileprivate weak var secondLine: UIView!
    @IBOutlet fileprivate weak var secondLabel: UILabel!
    @IBOutlet fileprivate weak var thirdLine: UIView!
    @IBOutlet fileprivate weak var thirdLabel: UILabel!
    @IBOutlet fileprivate weak var fourthLine: UIView!
    @IBOutlet fileprivate weak var fourthLabel: UILabel!
    @IBOutlet fileprivate weak var fifthLine: UIView!
    @IBOutlet fileprivate weak var fifthLabel: UILabel!
    @IBOutlet fileprivate weak var sixLine: UIView!
    @IBOutlet fileprivate weak var sixLabel: UILabel!
    
    fileprivate var lines: [UIView] {
        return [firstLine, secondLine, thirdLine, fourthLine, fifthLine, sixLine]
    }
    
    fileprivate var labels: [UILabel] {
        return [firstLabel, secondLabel, thirdLabel, fourthLabel, fifthLabel, sixLabel]
    }
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension SendCodeViewController {
    
    fileprivate func configureController() {
        navigationItem.backBarButtonItem = nil
        codeField.becomeFirstResponder()
        descrLabel.text = "SMS с кодом было отправлено на номер " + (phone ?? "")
    }
    
}

// MARK: - UI Actions

extension SendCodeViewController {
    
    @IBAction func codeFieldTextChanged(_ sender: UITextField) {
        let code = codeField.text ?? ""
        
        let charactersCount = code.count
        
        if charactersCount > 0 && charactersCount < 6 {
            lines[charactersCount - 1].isHidden = true
            labels[charactersCount - 1].text = code[charactersCount - 1]
            
            lines[charactersCount].isHidden = false
            labels[charactersCount].text = ""
        }
        else if charactersCount == 0 {
            lines[0].isHidden = false
            labels[0].text = ""
        }
        
        if charactersCount == 6 {
            lines[5].isHidden = true
            labels[5].text = code[5]
            
            let container = RegistrationContainer(phone: phone!, number: code)
            let service = ServiceLocator.shared.authService
            service.sendCode(with: container, completion: { [weak self] (info, error) in
                guard let this = self else { return }
                
                guard let responseInfo = info, error == nil else {
                    UIAlertController.showErrorAlert(in: this)
                    return
                }
                
                switch responseInfo.code {
                case .success:
                    this.updateFunctionalMode()
                case .error:
                    UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this)
                }
            })
        }
    }
}
