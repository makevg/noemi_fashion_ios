//
//  SignUpViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 16.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var phoneField: BaseTextField!
    @IBOutlet fileprivate weak var sendButton: ShadowButton!
    @IBOutlet fileprivate weak var checkView: CheckView!
    
    fileprivate let showSendCodeSegueIdentifier = "showSendCode"
    fileprivate var sendCodeVC: SendCodeViewController?
    
    fileprivate var phone: String?
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is SendCodeViewController {
            sendCodeVC = segue.destination as? SendCodeViewController
            sendCodeVC?.phone = phone
        }
    }
    
    // MARK: - Public
    
    func enableSendButton(_ enable: Bool) {
        sendButton.isEnabled = enable
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension SignUpViewController {
    
    fileprivate func configureController() {
        checkView.delegate = self
        enableSendButton(false)
        phoneField.becomeFirstResponder()
    }
    
}

// MARK: - UI Actions

extension SignUpViewController {
    
    @IBAction func unwindToSignUp(segue: UIStoryboardSegue) { }
    
    @IBAction func phoneFieldTextChanged(_ sender: BaseTextField) {
        let phoneIsEmpty = phoneField.text?.isEmpty ?? true
        let isCheked = checkView.isCheked
        enableSendButton(!phoneIsEmpty && isCheked)
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        phone = "+7" + phoneField.text!
        let service = ServiceLocator.shared.authService
        
        service.register(with: phone!) { [weak self] (info, error) in
            guard let this = self else { return }
            
            guard let responseInfo = info, error == nil else {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch responseInfo.code {
            case .success:
                this.performSegue(withIdentifier: this.showSendCodeSegueIdentifier, sender: nil)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this)
            }
        }
    }
    
}

extension SignUpViewController: CheckViewDelegate {
    
    func checkViewStateChanged(_ cheked: Bool) {
        let phoneIsEmpty = phoneField.text?.isEmpty ?? true
        let isCheked = checkView.isCheked
        print(isCheked)
        enableSendButton(!phoneIsEmpty && isCheked)
    }
    
}
