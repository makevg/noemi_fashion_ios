//
//  NewsCardHeaderView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 26/09/2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

final class NewsCardHeaderView: UIView {
	
	// MARK: - Properties
	
	private var imageView: UIImageView = {
		let imageView = UIImageView()
		imageView.contentMode = .scaleAspectFill
		imageView.clipsToBounds = true
		imageView.translatesAutoresizingMaskIntoConstraints = false
		return imageView
	}()
	
	private var titleLabel: UILabel = {
		let label = UILabel()
		label.sizeToFit()
		label.textColor = .white
		label.font = UIFont.systemFont(ofSize: 20)
		label.translatesAutoresizingMaskIntoConstraints = false
		return label
	}()
	
	// MARK: - Init
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		configureSubviews()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		configureSubviews()
	}
	
	// MARK: - Private
	
	private func configureSubviews() {
		addSubview(imageView)
		addSubview(titleLabel)
		
		imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
		imageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
		imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
		imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
		
		titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
		titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
	}
	
	func configure(with news: News) {
		titleLabel.text = news.name
		imageView.kf.setImage(with: URL(string: news.image),
													placeholder: UIImage(named: "look_placeholder"),
													progressBlock: nil,
													completionHandler: nil)
	}
	
}
