//
//  NewsCardVC.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.09.2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

final class NewsCardVC: BaseViewController {
	
	// MARK: - Properties
	
	@IBOutlet private weak var tableView: UITableView!
	private var headerView: NewsCardHeaderView!
	
	fileprivate var news: News?
	fileprivate var newsId: Int?
	
	private lazy var closeButton: UIBarButtonItem = {
		return UIBarButtonItem(title: "Закрыть",
													 style: .plain,
													 target: self,
													 action: #selector(closeButtonTapped))
	}()
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		configureController()
	}
	
	// MARK: - Private
	
	private func configureController() {
		configureTableView()
		
		if let news = news {
			headerView.configure(with: news)
		} else if let newsId = newsId {
			let service = ServiceLocator.shared.newsService
			service.getNews(with: newsId) { [weak self] news, serviceCode, error in
				guard let self = self else { return }
				
				guard let news = news,
					let code = serviceCode,
					error == nil else
				{
					UIAlertController.showErrorAlert(in: self)
					return
				}
				
				switch code {
				case .success:
					self.news = news
					self.headerView.configure(with: news)
					self.tableView.reloadData()
				case .error:
					UIAlertController.showAlert(with: "Ошибка",
																			message: code.rawValue,
																			in: self)
				}
			}
		}
	}
	
	private func configureTableView() {
		tableView.tableFooterView = UIView()
		tableView.rowHeight = UITableView.automaticDimension
		tableView.estimatedRowHeight = 40
		tableView.dataSource = self
		tableView.delegate = self
		
		let headerSize = CGSize(width: tableView.frame.size.width, height: 300)
		headerView = NewsCardHeaderView(frame: CGRect(origin: .zero, size: headerSize))
		tableView.tableHeaderView = headerView
		
//		tableView.contentInset = UIEdgeInsets(top: 300, left: 0, bottom: 0, right: 0)
//		tableView.contentOffset = CGPoint(x: 0, y: -300)
		
		
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
	}
	
	@objc private func closeButtonTapped() {
		dismiss(animated: true, completion: nil)
	}
	
	func configure(with news: News) {
		self.news = news
	}
	
	func configure(with newsId: Int) {
		self.newsId = newsId
	}
	
	func presentModal(presentingController: UIViewController) {
		navigationItem.leftBarButtonItem = closeButton
		let navVC = UINavigationController(rootViewController: self)
		presentingController.present(navVC, animated: true, completion: nil)
	}
	
}

// MARK: - UITableViewDataSource

extension NewsCardVC: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCardCell", for: indexPath) as! NewsCardCell
		if let news = news {
			cell.configure(with: news)
		}
		
		return cell
	}
	
}

// MARK: - UITableViewDelegate

extension NewsCardVC: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		var offset = scrollView.contentOffset.y / 150
		
		if offset < 1 {
			offset = 1
			let color = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
			navigationController?.navigationBar.backgroundColor = color
		} else {
			let color = UIColor(red: 1, green: 1, blue: 1, alpha: offset)
			navigationController?.navigationBar.backgroundColor = color
		}
	}
	
}

