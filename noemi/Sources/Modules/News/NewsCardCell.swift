//
//  NewsCardCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 26/09/2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit

final class NewsCardCell: UITableViewCell {
	
	// MARK: - Properties
	
	@IBOutlet private weak var dateLabel: UILabel!
	@IBOutlet private weak var contentLabel: UILabel!
	
	// MARK: - Override
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	func configure(with news: News) {
		dateLabel.text = Date(timeIntervalSince1970: news.date).stringDate
		contentLabel.text = news.content
	}

}
