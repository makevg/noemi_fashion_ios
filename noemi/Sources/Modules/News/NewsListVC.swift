//
//  NewsListVC.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.09.2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit

final class NewsListVC: BaseViewController {
	
	// MARK: - Properties
	
	@IBOutlet private weak var tableView: UITableView!
	fileprivate var news = [News]()
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		configureTableView()
		
		let service = ServiceLocator.shared.newsService
		service.getNewsList { [weak self] (news, code, error) in
			guard let news = news,
				let serviceCode = code,
				error == nil else
			{
				return
			}
			
			switch serviceCode {
			case .success:
				self?.updateUi(with: news)
			case .error:
				UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: self!)
			}
		}
	}
	
	private func configureTableView() {
		tableView.delegate = self
		tableView.dataSource = self
		tableView.tableFooterView = UIView()
	}
	
	private func updateUi(with news: [News]) {
		self.news = news
		showEmptyView(news.isEmpty)
		tableView.reloadData()
	}
	
	private func showEmptyView(_ show: Bool) {
		if show {
			let emptyView = EmptyView.emptyView
			emptyView.emptyText = "Нет новостей"
			tableView.backgroundView = emptyView
		} else {
			tableView.backgroundView = nil
		}
	}

}

// MARK: - UITableViewDataSource

extension NewsListVC: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return news.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
		cell.configure(with: news[indexPath.row])
		
		return cell
	}
	
}

// MARK: - UITableViewDelegate

extension NewsListVC: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		let vc = UIStoryboard.News.newsCardViewController
		vc.configure(with: news[indexPath.row])
		navigationController?.pushViewController(vc, animated: true)
	}
	
}
