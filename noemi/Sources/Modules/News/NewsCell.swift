//
//  NewsCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25/09/2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

final class NewsCell: UITableViewCell {
	
	// MARK: - Properties
	
	@IBOutlet private weak var newsImageView: UIImageView!
	@IBOutlet private weak var dateLabel: UILabel!
	@IBOutlet private weak var nameLabel: UILabel!
	@IBOutlet private weak var annotationLabel: UILabel!
	
	// MARK: - Override
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	func configure(with news: News) {
		nameLabel.text = news.name
		dateLabel.text = Date(timeIntervalSince1970: news.date).stringDate
		annotationLabel.text = news.annotation
		newsImageView.kf.setImage(with: URL(string: news.image),
															placeholder: UIImage(named: "look_placeholder"),
															progressBlock: nil,
															completionHandler: nil)
	}

}
