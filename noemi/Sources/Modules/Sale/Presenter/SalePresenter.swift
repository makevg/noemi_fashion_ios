//
//  SalePresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class SalePresenter {
    
    // MARK: - Properties
    
    weak var view: SaleViewController!
    var interactor: SaleInteractor!
    
    private let service = ServiceLocator.shared.profileSrevice
    private let shareManager = NoemiShareManager()
    
    // MARK: - Public
    
    func viewWillAppearEvent() {
        if AuthorizationManager.shared.accessToken == "" {
            view.showNotAuthorizedView(true)
        } else {
            view.showNotAuthorizedView(false)
            view.showActivityIndicator(true)
            interactor.obtainProfile { [weak self] (profile, code, error) in
                guard let this = self else { return }
                
                guard let info = profile,
                    let serviceCode = code,
                    error == nil else
                {
                    UIAlertController.showErrorAlert(in: this.view)
                    return
                }
                
                this.view.showActivityIndicator(false)
                switch serviceCode {
                case .success:
									this.view.update(with: info)
                case .error:
                    UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
                }
            }
        }
    }
    
    func didSelectGiveSaleCell() {
        service.getSaleCode { [weak self] (saleCode, code, error) in
            guard let this = self else { return }
            
            guard let saleCode = saleCode,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this.view)
                return
            }
            
            switch serviceCode {
            case .success:
                let text = "\(saleCode)\nАктивируй код в приложении noemi и получи скидку в магазине. Подробнее на noemifashion.com"
                this.shareManager.showActivityController(with: [text], in: this.view)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
            }
        }
    }
    
    func didSelectUseSaleCell() {
        interactor.getSaleCode { [weak self] (saleCode, code, error) in
            guard let this = self else { return }
            
            guard let saleCode = saleCode,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this.view)
                return
            }
            
            switch serviceCode {
            case .success:
                UIAlertController.showAlert(with: "Код для получения скидки",
                                            message: "\(saleCode)",
                    in: this.view)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
            }
        }
    }
    
}

// MARK: - NotAuthorizedViewDelegate

extension SalePresenter: NotAuthorizedViewDelegate {
    
    func notAuthorizedViewSignUpButtonTapped() {
        let signInVC = UIStoryboard.SignIn.initialController
        view.navigationController?.pushViewController(signInVC, animated: true)
    }
    
}
