//
//  SaleViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 22.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class SaleViewController: BaseViewController {
    
    fileprivate enum RowType: Int {
        case activateSale
        case giveSale
        case useSale
        case giveBonuses
        case howToSaveBonuses
    }
    
    fileprivate struct SegueIdentifiers {
        static let showHowToSaveBonusesIdentifier = "showHowToSaveBonuses"
        static let showActivateFriendCodeIdentifier = "showActivateFriendCode"
        static let showGiveBonusesIdentifier = "showGiveBonuses"
    }
    
    // MARK: - Properties
	
	@IBOutlet fileprivate weak var tableView: UITableView!
	@IBOutlet fileprivate weak var saleLabel: UILabel!
	@IBOutlet fileprivate weak var hiddenBonusesLabel: UILabel!
	@IBOutlet fileprivate weak var bonusesLabel: UILabel!
	@IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet fileprivate weak var notAuthorizedView: NotAuthorizedView!
	
	fileprivate var presenter: SalePresenter!
	
	fileprivate let cellIdentifier = "SaleTableViewCell"
	fileprivate let rowTitles = ["Активировать скидку друга",
															 "Подарить код на скидку",
															 "Использовать скидку",
															 "Подарить бонусные баллы",
															 "Как копить бонусы?"]
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        build()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        saleLabel.text = ""
        bonusesLabel.text = ""
        presenter.viewWillAppearEvent()
    }
    
    // MARK: - Public
    
    func build() {
        let presenter = SalePresenter()
        let interactor = SaleInteractor()
        presenter.view = self
        presenter.interactor = interactor
        interactor.presenter = presenter
        self.presenter = presenter
    }
	
	func update(with info: Profile) {
		saleLabel.text = "Моя скидка - \(info.discount)%"
		hiddenBonusesLabel.text = "Неактивные баллы - \(info.hiddenPoints)"
		bonusesLabel.text = "Активные баллы - \(info.points)"
	}
    
    func showActivityIndicator(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    func showNotAuthorizedView(_ show: Bool) {
        if show {
					view.bringSubviewToFront(notAuthorizedView)
        } else {
					view.bringSubviewToFront(tableView)
        }
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension SaleViewController {
    
    fileprivate func configureController() {
        tableView.dataSource = self
        tableView.delegate = self
        notAuthorizedView.delegate = presenter
    }
    
}

// MARK: - UITableViewDataSource

extension SaleViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = rowTitles[indexPath.row]
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension SaleViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        switch row {
        case RowType.activateSale.rawValue:
            performSegue(withIdentifier: SegueIdentifiers.showActivateFriendCodeIdentifier, sender: nil)
        case RowType.giveSale.rawValue:
            presenter.didSelectGiveSaleCell()
        case RowType.useSale.rawValue:
            presenter.didSelectUseSaleCell()
        case RowType.giveBonuses.rawValue:
            performSegue(withIdentifier: SegueIdentifiers.showGiveBonusesIdentifier, sender: nil)
        case RowType.howToSaveBonuses.rawValue:
            performSegue(withIdentifier: SegueIdentifiers.showHowToSaveBonusesIdentifier, sender: nil)
        default:
            break
        }
    }
    
}
