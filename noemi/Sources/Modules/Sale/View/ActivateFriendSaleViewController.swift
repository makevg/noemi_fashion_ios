//
//  ActivateFriendSaleViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 22.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ActivateFriendSaleViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var codeField: UITextField!
    @IBOutlet fileprivate weak var activateButton: ShadowButton!
    fileprivate let service = ServiceLocator.shared.profileSrevice
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()

        configureController()
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension ActivateFriendSaleViewController {
    
    fileprivate func configureController() {
        codeField.becomeFirstResponder()
        enableActivateButton(false)
    }
    
    fileprivate func enableActivateButton(_ enable: Bool) {
        activateButton.isEnabled = enable
    }
    
}

// MARK: - UI Actions

extension ActivateFriendSaleViewController {
    
    @IBAction func codeFieldTextDidChange(_ sender: UITextField) {
        let code = sender.text ?? ""
        enableActivateButton(!code.isEmpty)
    }
    
    @IBAction func activateButtonTapped(_ sender: Any) {
        let code = codeField.text ?? ""
        service.getFriendSale(with: code) { [weak self] (info, error) in
            guard let this = self else { return }
            
            guard let responseInfo = info,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch responseInfo.code {
            case .success:
                this.navigationController?.popViewController(animated: true)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this)
            }
        }
    }
    
}
