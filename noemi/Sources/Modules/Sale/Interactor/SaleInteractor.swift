//
//  SaleInteractor.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class SaleInteractor {
    
    // MARK: - Properties
    
    weak var presenter: SalePresenter!
    private let service = ServiceLocator.shared.profileSrevice
    
    // MARK: - Public
    
    func obtainProfile(with completion: @escaping ProfileCompletion) {
        service.getProfileInfo(with: completion)
    }
    
    func getSaleCode(with completion: @escaping GetSaleCodeCompletion) {
        service.getUserSaleCode(with: completion)
    }
    
}
