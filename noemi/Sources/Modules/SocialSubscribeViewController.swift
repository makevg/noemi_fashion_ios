//
//  SocialSubscribeViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

final class SocialSubscribeViewController: BaseViewController, LocationManagerProvider {
    
    fileprivate enum SocialGroupType {
        case instagram
        case fb
        case vk
        case ok
    }
	
	fileprivate var selectedGroupType: SocialGroupType?
	fileprivate var findedRegions = [Region]()
	
	fileprivate let service = ServiceLocator.shared.socialsService

    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
		
		locationManager.requestWhenInUseAuthorization()
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension SocialSubscribeViewController {
	
	fileprivate func subscribe() {
		if let location = locationManager.location?.coordinate {
			let coordinates = "\(location.latitude),\(location.longitude)"
			searchSoials(by: coordinates)
		} else {
			getRegions()
		}
	}
    
	fileprivate func subscribeToGroup(_ socialGroupType: SocialGroupType, region: Region) {
        var urlStr = ""
		switch socialGroupType {
		case .instagram:
			urlStr = region.instagram
		case .fb:
			urlStr = region.facebook
		case .vk:
			urlStr = region.vk
		case .ok:
			urlStr = region.ok
		}
		
		UrlOpener.open(urlStr)
	}
	
	private func searchSoials(by coordinates: String) {
		service.searchSocials(by: coordinates) { [weak self] region, code, error in
			guard let this = self else { return }
			
			guard let region = region,
				let serviceCode = code,
				error == nil else
			{
				UIAlertController.showErrorAlert(in: this)
				return
			}
			
			switch serviceCode {
			case .success:
				guard let type = this.selectedGroupType else { return }
				this.subscribeToGroup(type, region: region)
			case .error:
				UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
			}
		}
	}
	
	private func getRegions() {
		service.getRegions { [weak self] regions, code, error in
			guard let this = self else { return }
			
			guard let regions = regions,
				let serviceCode = code,
				error == nil else
			{
				UIAlertController.showErrorAlert(in: this)
				return
			}
			
			switch serviceCode {
			case .success:
				this.showPicker(with: regions)
			case .error:
				UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
			}
		}
	}
	
	private func showPicker(with regions: [Region]) {
		findedRegions = regions
		let regionsName = regions.map { $0.name }
		
		let picker = UIStoryboard.DatePicker.dataPickerController
		picker.data = regionsName
		picker.delegate = self
		present(picker, animated: true, completion: nil)
	}
	
}

// MARK: - UI Actions

extension SocialSubscribeViewController {
    
    @IBAction func instagramButtonTapped(_ sender: Any) {
		selectedGroupType = .instagram
		subscribe()
    }
    
    @IBAction func vkButtonTapped(_ sender: Any) {
		selectedGroupType = .vk
		subscribe()
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
		selectedGroupType = .fb
		subscribe()
    }
    
    @IBAction func okButtonTapped(_ sender: Any) {
        selectedGroupType = .ok
		subscribe()
    }
    
}

// MARK: - DataPickerCotrollerDelegate

extension SocialSubscribeViewController: DataPickerCotrollerDelegate {
	
	func dataPickerCotroller(controller: DataPickerViewController, select data: String) {		
		if let socialType = selectedGroupType,
			let region = findedRegions.first(where: { $0.name == data }) {
			subscribeToGroup(socialType, region: region)
		}
	}
	
}
