//
//  ShopsListViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

struct StoresSection {
    let title: String
    let stores: [Store]
}

class ShopsListViewController: BaseViewController, LocationManagerProvider {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var searchBar: UISearchBar!
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var presenter: ShopsListPresenter!
    fileprivate var stores = [Store]()
    
    fileprivate var sections = [StoresSection]()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppearEvent()
    }
    
    // MARK: - Public
    
    func build() {
        let presenter = ShopsListPresenter()
        let interactor = ShopsListInteractor()
        presenter.view = self
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        self.presenter = presenter
    }
    
    func updateUI(with stores: [Store]) {
        self.stores = stores
        showEmptyView(stores.isEmpty)
        tableView.reloadData()
    }
    
    func updateUI(with sections: [StoresSection]) {
        self.sections = sections
        showEmptyView(sections.isEmpty)
        tableView.reloadData()
    }
    
    func showEmptyView(_ show: Bool) {
        if show {
            let emptyView = EmptyView.emptyView
            emptyView.emptyText = "Нет магазинов"
            tableView.backgroundView = emptyView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func showActivityIndicator(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

}

// MARK: - Extensions

// MARK: - Private

extension ShopsListViewController {
    
    fileprivate func configureController() {
        searchBar.delegate = self
        configureTableView()
        configureLocationManager()
        build()
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
    private func configureLocationManager() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
}

// MARK: - UISearchBarDelegate

extension ShopsListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.searchBarTextDidChange(searchText)
    }
    
}

// MARK: - UITableViewDataSource

extension ShopsListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].stores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ShopTableViewCell.identifier) as! ShopTableViewCell
        let store = sections[indexPath.section].stores[indexPath.row]
        cell.configure(with: store)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
    
}

// MARK: - UITableViewDelegate

extension ShopsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? ShopTableViewCell else { return }
        presenter.selectShopListCell(cell)
    }
    
}

