//
//  ShopTableViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import CoreLocation

class ShopTableViewCell: BaseTableViewCell, LocationManagerProvider {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var locationLabel: UILabel!
    @IBOutlet fileprivate weak var distanceLabel: UILabel!
    
    var store: Store?
    
    // MARK: - Override
    
    override class var cellHeight: CGFloat {
        return 44
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Public
    
    func configure(with store: Store) {
        self.store = store
        
        nameLabel.text = store.name
        timeLabel.text = store.work
        locationLabel.text = store.address
        distanceLabel.text = ""
        
        if let location = locationManager.location,
            let latitude = CLLocationDegrees(store.latitude),
            let longitude = CLLocationDegrees(store.longitude)
        {
            let officeLocation = CLLocation(latitude: latitude,
                                            longitude: longitude)
            let distance = location.distanceInKilometersString(from: officeLocation)
            distanceLabel.text = distance + " км"
        }
    }
    
}
