//
//  ShopsListInteractor.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class ShopsListInteractor {
    
    // MARK: - Properties
    
    weak var presenter: ShopsListPresenter!
    private let service = ServiceLocator.shared.storesService
    
    // MARK: - Public
    
    func obtainStores(with completion: @escaping StoresCompletion) {
        service.getStores(with: completion)
    }
    
    func searchStores(with storeName: String, completion: @escaping StoresCompletion) {
        service.searchStores(with: storeName, completion: completion)
    }

}
