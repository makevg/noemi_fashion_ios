//
//  ShopsListPresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 18.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ShopsListPresenter {

    // MARK: - Properties
    
    weak var view: ShopsListViewController!
    var interactor: ShopsListInteractor!
    
    // MARK: - Public
    
    func viewWillAppearEvent() {
        view.showActivityIndicator(true)
        interactor.obtainStores { [weak self] (stores, code, error) in
            guard let this = self else { return }
            this.view.showActivityIndicator(false)
            this.updateUI(with: stores, code: code, error: error)
        }
    }
    
    func searchBarTextDidChange(_ searchText: String) {
        view.showActivityIndicator(true)
        interactor.searchStores(with: searchText) { [weak self] (stores, code, error) in
            guard let this = self else { return }
            this.view.showActivityIndicator(false)
            this.updateUI(with: stores, code: code, error: error)
        }
    }
    
    func selectShopListCell(_ cell: ShopTableViewCell) {
        guard let store = cell.store else { return }
        let mapViewController = UIStoryboard.Shops.mapViewController
        mapViewController.build(with: store)
        view.navigationController?.pushViewController(mapViewController, animated: true)
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension ShopsListPresenter {
    
    private func prepareSections(with stores: [Store]) -> [StoresSection] {
        var sections = [StoresSection]()
        let cities = Set(stores.compactMap { $0.city })
        for city in cities {
            let storesInCity = stores.filter { $0.city == city }
            let section = StoresSection(title: city, stores: storesInCity)
            sections.append(section)
        }
        return sections
    }
    
    fileprivate func updateUI(with stores: [Store]?, code: ServiceCode?, error: ServiceError?) {
        guard let stores = stores,
            let serviceCode = code,
            error == nil else
        {
            view.showActivityIndicator(false)
            UIAlertController.showErrorAlert(in: view)
            return
        }
        
        view.showActivityIndicator(false)
        
        switch serviceCode {
        case .success:
            let sections = prepareSections(with: stores)
            view.updateUI(with: sections)
        case .error:
            UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: view)
        }
    }
    
}
