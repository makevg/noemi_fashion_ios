//
//  LooksListViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class LooksListViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var looksTitleLabel: UILabel!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var presenter: LooksListPresenter?
    fileprivate var looks = [LookItem]()
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter?.viewWillAppearEvent()
    }
    
    // MARK: - Public
    
    func build(with presenter: LooksListPresenter) {
        let presenter = presenter
        presenter.view = self
        self.presenter = presenter
    }
    
    func updateUI(with looks: [LookItem]) {
        self.looks = looks
        showEmptyView(looks.isEmpty)
        tableView.reloadData()
    }
    
    func updateTitle(_ title: String) {
        looksTitleLabel.text = title
    }
    
    func showEmptyView(_ show: Bool) {
        if show {
            let emptyView = EmptyView.emptyView
            emptyView.emptyText = "Ничего не найдено"
            tableView.backgroundView = emptyView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func showActivityIndicator(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
}

// MARK: - Extensions

// MARK: - Private

extension LooksListViewController {
    
    fileprivate func configureController() {
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
}

// MARK: - UITableViewDataSource

extension LooksListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return looks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LookTableViewCell.identifier) as! LookTableViewCell
        cell.configure(with: looks[indexPath.row])
        cell.delegate = presenter
        return cell
    }
    
}
