//
//  CreateLookViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 24.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol CreateLookViewControllerDelegate: class {
    func createLookControllerClosed()
}

class CreateLookViewController: BaseViewController {
    
    // MARK: - Properties
    
    weak var delegate: CreateLookViewControllerDelegate?
    
    fileprivate var picker: UIImagePickerController!
    
    fileprivate var closeButton: UIBarButtonItem {
        let button = UIBarButtonItem(title: "Закрыть",
                                     style: .plain,
                                     target: self,
                                     action: #selector(closeButtonAction))
        return button
    }
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    // MARK: - UI Actions
    
    @objc
    private func closeButtonAction() {
        delegate?.createLookControllerClosed()
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension CreateLookViewController {
    
    fileprivate func configureController() {
        navigationItem.leftBarButtonItem = closeButton
        
        picker =  UIImagePickerController()
        picker.delegate = self
        picker.modalPresentationStyle = .popover
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
        }
    }
    
    fileprivate func takePhoto() {
        UIImagePickerController.obtainPermission(for: .camera, successHandler: { [weak self] in
            guard let this = self else { return }
            this.present(this.picker, animated: true, completion: nil)
        }) { [weak self] in
            guard let this = self else { return }
            let message = "Включите разрешение в настройках для использования камеры."
            UIAlertController.showOpenSettingsAlert(with: message, in: this)
        }
    }
    
}

// MARK: - UI Actions

extension CreateLookViewController {
    
    @IBAction func shareDiscountButtonTapped(_ sender: Any) {
        print(#function)
    }
    
    @IBAction func shareLookButtonTapped(_ sender: Any) {
        let actionSheet = UIAlertController(title: "", message: "Выберите действие", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Сделать LOOK", style: .default, handler: { [weak self] (_) in
            guard let this = self else { return }
            this.takePhoto()
        }))
        actionSheet.addAction(UIAlertAction(title: "Поделиться LOOK'ом", style: .default, handler: { [weak self] (_) in
            guard let this = self else { return }
            let myLooksViewController = UIStoryboard.Looks.myLooksViewController
            this.navigationController?.pushViewController(myLooksViewController, animated: true)
        }))
        actionSheet.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        if let popoverController = actionSheet.popoverPresentationController {
            popoverController.sourceView = view
            popoverController.sourceRect = view.bounds
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
}

// MARK: - UIImagePickerControllerDelegate

extension CreateLookViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController,
														 didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		picker.dismiss(animated: true, completion: nil)
		if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
			NoemiAlbum.shared.saveImage(image)
		}
	}
    
}
