//
//  LookTableViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

protocol LookTableViewCellDelegate: class {
    func lookCellShareButtonTapped(_ cell: LookTableViewCell)
}

class LookTableViewCell: BaseTableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var lookImageView: UIImageView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    @IBOutlet fileprivate weak var sizeLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    @IBOutlet fileprivate weak var shareButton: ShadowButton!
    
    weak var delegate: LookTableViewCellDelegate?
    
    var look: LookItem?
    
    var lookImage: UIImage? {
        return lookImageView.image
    }
    
    // MARK: - Override
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        clearSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clearSubviews()
    }
    
    // MARK: - Private
    
    private func clearSubviews() {
        nameLabel.text = nil
        dateLabel.text = nil
        sizeLabel.text = nil
        priceLabel.text = nil
        lookImageView.image = nil
    }

    // MARK: - Public
    
    func configure(with look: LookItem) {
        self.look = look
        
        nameLabel.text = look.name
        dateLabel.text = Date(timeIntervalSince1970: look.purchaseDate).stringDate
        sizeLabel.text = "размер " + look.size
        priceLabel.text = look.price + " руб"
        lookImageView.kf.setImage(with: URL(string: look.imageUrl),
                                  placeholder: UIImage(named: "look_placeholder"),
                                  progressBlock: nil,
                                  completionHandler: nil)
    }
    
    // MARK: - UI Actions
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        delegate?.lookCellShareButtonTapped(self)
    }
    
}
