//
//  LooksListPresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class LooksListPresenter {
    
    // MARK: - Properties
    
    weak var view: LooksListViewController!
    
    // MARK: - Public
    
    func viewWillAppearEvent() {
        
    }
    
}

// MARK: - Extensions

// MARK: - LookTableViewCellDelegate

extension LooksListPresenter: LookTableViewCellDelegate {
    
    func lookCellShareButtonTapped(_ cell: LookTableViewCell) {
        guard let look = cell.look, let lookImage = cell.lookImage else {
            return
        }
        
        NoemiShareManager.shared.showShareActionSheet(with: look, image: lookImage, in: view)
    }
    
}
