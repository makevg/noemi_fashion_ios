//
//  MyPurchasesPresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class MyPurchasesPresenter: LooksListPresenter {
    
    // MARK: - Properties
    
    private let service = ServiceLocator.shared.looksService
    
    // MARK: - Override
    
    override func viewWillAppearEvent() {
        view?.showActivityIndicator(true)
        view?.updateTitle("Мои покупки")
        service.getPurchasesList { [weak self] (looks, code, error) in
            guard let this = self else { return }
            
            guard let looks = looks,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this.view)
                return
            }
            
            this.view.showActivityIndicator(false)
            switch serviceCode {
            case .success:
                this.view.updateUI(with: looks)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
            }
        }
    }
    
}
