//
//  ProfileViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 06.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    // MARK: - Properties
    
    fileprivate var presenter: ProfilePresenter!
    fileprivate var profileFormViewController: ProfileFormViewController!
    
    var profileInfoContainer: ProfileContainer {
        let name = profileFormViewController.name
        let surname = profileFormViewController.lastName
        let birthday = profileFormViewController.birthday
        let gender = profileFormViewController.gender == "Женский" ? 0 : 1
        let phone = profileFormViewController.phone
        let email = profileFormViewController.email
        var password = ""
        if profileFormViewController.password != AuthorizationManager.shared.password {
            password = profileFormViewController.password
        }
        
        return ProfileContainer(name: name,
                                surname: surname,
                                birthday: birthday,
                                gender: gender,
                                password: password,
                                phone: phone,
                                email: email,
                                card: "")
        
    }
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        build()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppear()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ProfileFormViewController {
            let vc = segue.destination as! ProfileFormViewController
            vc.delegate = presenter
            profileFormViewController = vc
        }
    }
    
    // MARK: - Public
    
    func build() {
        let presenter = ProfilePresenter()
        let interactor = ProfileInteractor()
        presenter.view = self
        presenter.interactor = interactor
        interactor.presenter = presenter
        self.presenter = presenter
    }
    
    func updateUI(with profileInfo: Profile) {
        profileFormViewController.configure(with: profileInfo)
    }
    
}

// MARK: - Extension

extension ProfileViewController {
    
    @IBAction func saveButtonTapped(_ sender: Any) {
        presenter.didTapSaveButton()
    }
    
}
