//
//  ProfileInteractor.swift
//  noemi
//
//  Created by Maximychev Evgeny on 20.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class ProfileInteractor {
    
    // MARK: - Properties
    
    weak var presenter: ProfilePresenter!
    private let service = ServiceLocator.shared.profileSrevice
    
    // MARK: - Public
    
    func obtainProfileInfo(completion: @escaping ProfileCompletion) {
        service.getProfileInfo(with: completion)
    }
    
    func saveProfileInfo(with container: ParametrizableType, completion: @escaping BaseServiceCompletion) {
        service.setProfileInfo(with: container, completion: completion)
    }
    
}
