//
//  ProfileFormViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 06.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol ProfileFormViewControllerDelegate: class {
    func profileFormControllerDidTapLogoutButton(_ controller: ProfileFormViewController)
}

class ProfileFormViewController: UITableViewController {
    
    fileprivate enum SectionType: Int {
        case loginInfo
        case personalInfo
        case phone
        case logout
    }
    
    fileprivate enum LoginInfoSectionRowType: Int {
        case email
        case password
    }
    
    fileprivate enum PersonalInfoSectionRowType: Int {
        case firstName
        case lastName
        case gender
        case birthday
    }
    
    fileprivate enum PhoneSectionRowType: Int {
        case phone
    }
    
    // MARK: - Properties
    
    weak var delegate: ProfileFormViewControllerDelegate?
    
    @IBOutlet fileprivate weak var emailField: BaseTextField!
    @IBOutlet fileprivate weak var passwordField: BaseTextField!
    @IBOutlet fileprivate weak var nameField: BaseTextField!
    @IBOutlet fileprivate weak var lastNameFIeld: BaseTextField!
    @IBOutlet fileprivate weak var genderLabel: UILabel!
    @IBOutlet fileprivate weak var birthdayLabel: UILabel!
    @IBOutlet fileprivate weak var phoneLabel: UILabel!
    
    fileprivate var datePicker: DatePickerCotroller {
        let picker = UIStoryboard.DatePicker.datePickerController
        picker.delegate = self
        return picker
    }
    
    fileprivate var genderPicker: DataPickerViewController {
        let picker = UIStoryboard.DatePicker.dataPickerController
        picker.delegate = self
        picker.data = ["Женский", "Мужской"]
        return picker
    }
    
    var email: String {
        return emailField.text ?? ""
    }
    
    var password: String {
        return passwordField.text ?? ""
    }
    
    var name: String {
        return nameField.text ?? ""
    }
    
    var lastName: String {
        return lastNameFIeld.text ?? ""
    }
    
    var gender: String {
        return genderLabel.text ?? ""
    }
    
    var birthday: String {
        return birthdayLabel.text ?? ""
    }
    
    var phone: String {
        return phoneLabel.text ?? ""
    }
    
    // MARK: - Public
    
    func configure(with profileInfo: Profile) {
        emailField.text = profileInfo.email
        passwordField.text = AuthorizationManager.shared.password
        nameField.text = profileInfo.name
        lastNameFIeld.text = profileInfo.surname
        genderLabel.text = profileInfo.gender
        birthdayLabel.text = profileInfo.birthday
        phoneLabel.text = profileInfo.phone
    }
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = indexPath.section
        let row = indexPath.row
        
        switch section {
        case SectionType.personalInfo.rawValue:
            switch row {
            case PersonalInfoSectionRowType.gender.rawValue:
                present(genderPicker, animated: true, completion: nil)
            case PersonalInfoSectionRowType.birthday.rawValue:
                present(datePicker, animated: true, completion: nil)
            default:
                break
            }
        case SectionType.logout.rawValue:
            delegate?.profileFormControllerDidTapLogoutButton(self)
        default:
            break
        }
    }

}

// MARK: - Extensions

// MARK: - DatePickerCotrollerDelegate

extension ProfileFormViewController: DatePickerCotrollerDelegate {
    
    func datePickerCotroller(controller: DatePickerCotroller, select date: Date) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let dateStr = formatter.string(from: date)
        birthdayLabel.text = dateStr
    }
    
}

// MARK: - DataPickerViewController

extension ProfileFormViewController: DataPickerCotrollerDelegate {
    
    func dataPickerCotroller(controller: DataPickerViewController, select data: String) {
        genderLabel.text = data
    }
    
}
