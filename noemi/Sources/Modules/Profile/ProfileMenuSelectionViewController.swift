//
//  ProfileMenuSelectionViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ProfileMenuSelectionViewController: UITableViewController {
    
    private enum ProfileMenuRowType: Int {
        case myLooks
        case myPurchases
        case wishList
        case favoriteLookBooks
        case profileInfo
    }
    
    // MARK: - Override
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case ProfileMenuRowType.myPurchases.rawValue:
            showMyPurchases()
        case ProfileMenuRowType.wishList.rawValue:
            showWishList()
        case ProfileMenuRowType.favoriteLookBooks.rawValue:
            showFavoriteLooks()
        default:
            break
        }
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension ProfileMenuSelectionViewController {
    
    fileprivate func showMyPurchases() {
        let vc = UIStoryboard.Looks.looksListViewController
        vc.build(with: MyPurchasesPresenter())
        navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func showWishList() {
        let vc = UIStoryboard.Looks.looksListViewController
        vc.build(with: FavoritesLooksPresenter())
        navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func showFavoriteLooks() {
        let vc = UIStoryboard.Looks.favoriteLooksViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
