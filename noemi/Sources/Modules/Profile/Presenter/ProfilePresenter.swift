//
//  ProfilePresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 20.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ProfilePresenter {
    
    // MARK: - Properties
    
    weak var view: ProfileViewController!
    var interactor: ProfileInteractor!
    
    // MARK: - Public
    
    func viewWillAppear() {
        interactor.obtainProfileInfo { [weak self] (info, code, error) in
            guard let this = self else { return }
            
            guard let info = info,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this.view)
                return
            }
            
            switch serviceCode {
            case .success:
                this.view.updateUI(with: info)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
            }
        }
    }
    
    func didTapSaveButton() {
        let container = view.profileInfoContainer
        interactor.saveProfileInfo(with: container) { [weak self] (info, error) in
            guard let this = self else { return }
            
            guard let responseInfo = info,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this.view)
                return
            }
            
            switch responseInfo.code {
            case .success:
                UIAlertController.showAlert(with: "Внимание", message: "Данные обновлены", in: this.view)
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this.view)
            }
        }
    }
    
}

// MARK: - Extensions

// MARK: - ProfileFormViewControllerDelegate

extension ProfilePresenter: ProfileFormViewControllerDelegate {
    
    func profileFormControllerDidTapLogoutButton(_ controller: ProfileFormViewController) {
        AuthorizationManager.shared.clearAccessToken()
        view.updateFunctionalMode()
    }
    
}
