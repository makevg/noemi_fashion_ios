//
//  ProfileMainViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 07.07.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ProfileMainViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet private weak var notAuthorizedView: NotAuthorizedView!
    @IBOutlet private weak var containerView: UIView!
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        notAuthorizedView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if AuthorizationManager.shared.accessToken == "" {
					view.bringSubviewToFront(notAuthorizedView)
        } else {
					view.bringSubviewToFront(containerView)
        }
    }

}

// MARK: - Extensions

// MARK:- NotAuthorizedViewDelegate

extension ProfileMainViewController: NotAuthorizedViewDelegate {
    
    func notAuthorizedViewSignUpButtonTapped() {
        let signInVC = UIStoryboard.SignIn.initialController
        navigationController?.pushViewController(signInVC, animated: true)
    }
    
}
