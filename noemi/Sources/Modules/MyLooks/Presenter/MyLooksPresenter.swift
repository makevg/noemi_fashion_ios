//
//  MyLooksPresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class MyLooksPresenter {
    
    // MARK: - Properties
    
    weak var view: MyLooksViewController!
    var interactor: MyLooksInteractor!
    
    // MARK: - Public
    
    func viewWillAppearEvent() {
        UIImagePickerController.obtainPermission(for: .photoLibrary, successHandler: {
            self.view.showActivityIndicator(true)
            self.interactor.obtainMyLooks { [weak self] (assets) in
                guard let this = self, let assets = assets else { return }
                this.view.showActivityIndicator(false)
                this.view.updateUI(with: assets)
            }
        }) { [weak self] in
            guard let this = self else { return }
            let message = "Включите разрешение в настройках для просмотра Ваших Look'ов"
            UIAlertController.showOpenSettingsAlert(with: message, in: this.view)
        }
    }
    
}

// MARK: - Extensions

// MARK: - MyLookTableViewCellDelegate

extension MyLooksPresenter: MyLookTableViewCellDelegate {
    
    func myLookCellShareButtonTapped(_ cell: MyLookTableViewCell) {
        guard let lookImage = cell.lookImage else { return }
        NoemiShareManager.shared.showShareActionSheet(with: lookImage, in: view)
    }
    
}
