//
//  MyLookTableViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Photos

protocol MyLookTableViewCellDelegate: class {
    func myLookCellShareButtonTapped(_ cell: MyLookTableViewCell)
}

class MyLookTableViewCell: BaseTableViewCell {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var lookImageView: UIImageView!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    
    weak var delegate: MyLookTableViewCellDelegate?
    
    
    
    var lookImage: UIImage? {
        
        get {
            return lookImageView.image
        }
        
        set {
            lookImageView.image = newValue
        }
    }
    
    // MARK: - Override
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        clearSubviews()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        clearSubviews()
    }
    
    // MARK: - Private
    
    private func clearSubviews() {
        lookImageView.image = nil
        dateLabel.text = nil
    }
    
    // MARK: - Public
    
    func configure(with asset: PHAsset) {
        let imageManager = PHImageManager()
        imageManager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFill, options: nil) { (image, _) in
            self.lookImageView.image = image?.resizedImage()
        }
        
        if let date = asset.creationDate {
            dateLabel.text = date.stringDate
        }
    }

}

// MARK: - Extensions

// MARK: - UI Actions

extension MyLookTableViewCell {
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        delegate?.myLookCellShareButtonTapped(self)
    }
    
}

extension Date {
    
    var stringDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"
        return dateFormatter.string(from: self)
    }
    
}
