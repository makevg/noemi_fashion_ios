//
//  MyLooksViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Photos

class MyLooksViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var presenter: MyLooksPresenter!
    fileprivate var assets = [PHAsset]()
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        build()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
        presenter.viewWillAppearEvent()
    }

    // MARK: - Public
    
    func updateUI(with assets: [PHAsset]) {
        self.assets = assets
        showEmptyView(assets.isEmpty)
        tableView.reloadData()
    }
    
    func showEmptyView(_ show: Bool) {
        if show {
            let emptyView = EmptyView.emptyView
            emptyView.emptyText = "Нет Look'ов"
            tableView.backgroundView = emptyView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func showActivityIndicator(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension MyLooksViewController {
    
    fileprivate func build() {
        let presenter = MyLooksPresenter()
        let interactor = MyLooksInteractor()
        presenter.view = self
        presenter.interactor = interactor
        interactor.presenter = presenter
        self.presenter = presenter
    }
    
    fileprivate func configureController() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
    }
    
}

// MARK: - UITableViewDataSource

extension MyLooksViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return assets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = MyLookTableViewCell.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MyLookTableViewCell
        cell.configure(with: assets[indexPath.row])
        cell.delegate = presenter
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension MyLooksViewController: UITableViewDelegate {
    
}
