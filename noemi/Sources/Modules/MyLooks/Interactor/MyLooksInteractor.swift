//
//  MyLooksInteractor.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class MyLooksInteractor {
    
    // MARK: - Properties
    
    weak var presenter: MyLooksPresenter!
    
    // MARK: - Public
    
    func obtainMyLooks(with completion: @escaping ObtainNoemiAssetsCompletion) {
        NoemiAlbum.shared.obtainAssets(with: completion)
    }
    
}
