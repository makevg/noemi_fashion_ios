//
//  ScanProductViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ScanProductViewController: BaseViewController {
    
    fileprivate enum ViewState {
        case main
        case productFounded
        case productNotFounded
    }
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var mainView: UIView!
    @IBOutlet fileprivate weak var productSuccessView: ScanProductSuccessView!
    @IBOutlet fileprivate weak var noProductView: UIView!
    
    fileprivate let service = ServiceLocator.shared.looksService
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
		
		navigationItem.rightBarButtonItem = nil
        productSuccessView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        showView(with: .main)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is QRScannerViewController {
            let vc = segue.destination as! QRScannerViewController
            vc.delegate = self
        }
    }
    
    // MARK: - Private
    
    fileprivate func showView(with state: ViewState) {
        switch state {
        case .main:
					view.bringSubviewToFront(mainView)
        case .productFounded:
					view.bringSubviewToFront(productSuccessView)
        case .productNotFounded:
					view.bringSubviewToFront(noProductView)
        }
    }

	// MARK: - Actions
	
	@IBAction func closeButtonTapped(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	
}

// MARK: - Extensions

// MARK: - QRScannerViewControllerDelegate

extension ScanProductViewController: QRScannerViewControllerDelegate {
    
    func scanCompleted(_ code: String) {
//        let code = "A10143"
        service.getProduct(with: code) { [weak self] (looks, code, error) in
            guard let this = self else { return }
            
            guard let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch serviceCode {
            case .success:
                if let look = looks?.first {
                    this.productSuccessView.configure(with: look)
                    this.showView(with: .productFounded)
                } else {
                    this.showView(with: .productNotFounded)
                }
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
            }
        }
    }
    
}

// MARK: - ScanProductSuccessViewDelegate

extension ScanProductViewController: ScanProductSuccessViewDelegate {
    
    func productSuccessViewAddToFavoriteButtonTapped(_ view: ScanProductSuccessView) {
        if let productId = view.look?.uuid {
            let container = FavoriteContainer(productId: productId, status: true)
            service.setFavoriteProduct(with: container, completion: { [weak self] (info, error) in
                guard let this = self else { return }
                
                guard let responseInfo = info,
                    error == nil else
                {
                    UIAlertController.showErrorAlert(in: this)
                    return
                }
                
                switch responseInfo.code {
                case .success:
                    view.showAddToFavoriteButton(false)
                    UIAlertController.showAlert(with: "Внимание", message: "Товар добавлен в избранное", in: this)
                case .error:
                    UIAlertController.showAlert(with: "Ошибка", message: responseInfo.message ?? "", in: this)
                }
            })
        }
    }
    
    func productSuccessViewShareButtonTapped(_ view: ScanProductSuccessView) {
        if let lookItem = view.look, let lookImage = view.lookImage {
            NoemiShareManager.shared.showShareActionSheet(with: lookItem, image: lookImage, in: self)
        }
    }
    
}
