//
//  QRScannerViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol QRScannerViewControllerDelegate: class {
    func scanCompleted(_ code: String)
}

class QRScannerViewController: BaseViewController {

    // MARK: - Properties
    
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    
    fileprivate lazy var scanner: QRCode = {
        let qrScanner = QRCode()
        qrScanner.maxDetectedCount = 1
        qrScanner.currentDetectedCount = 1
        return qrScanner
    }()
    
    weak var delegate: QRScannerViewControllerDelegate?
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
		
        // start scan
        scanner.startScan()
    }
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        iconView.image = QRCode.generateImage("Hello world", avatarImage: nil)
//    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension QRScannerViewController {
    
    fileprivate func configureController() {
		scanner.prepareScan(view) { [weak self] code -> () in
            guard let this = self else { return }
            this.delegate?.scanCompleted(code)
            this.dismissController()
        }
        scanner.scanFrame = view.bounds
    }
    
    fileprivate func dismissController() {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - Actions

extension QRScannerViewController {
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismissController()
    }
}
