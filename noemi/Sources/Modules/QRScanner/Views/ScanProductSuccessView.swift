//
//  ScanProductSuccessView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 25.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

protocol ScanProductSuccessViewDelegate: class {
    func productSuccessViewAddToFavoriteButtonTapped(_ view: ScanProductSuccessView)
    func productSuccessViewShareButtonTapped(_ view: ScanProductSuccessView)
}

class ScanProductSuccessView: UIView {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate var mainView: UIView!
    @IBOutlet fileprivate weak var innerView: UIView!
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var nameLabel: UILabel!
    @IBOutlet fileprivate weak var sizesLabel: UILabel!
    @IBOutlet fileprivate weak var discountPriceLabel: UILabel!
    @IBOutlet fileprivate weak var priceLabel: UILabel!
    @IBOutlet fileprivate weak var pageIndicator: UIPageControl!
    @IBOutlet fileprivate weak var addToFavoriteButton: UIButton!
    
    weak var delegate: ScanProductSuccessViewDelegate?
    
    var look: LookItem?
    
    fileprivate var lookPreview: UIImage?
    var lookImage: UIImage? {
        return lookPreview
    }
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - Public
    
    func configure(with look: LookItem) {
        self.look = look
        
        nameLabel.text = look.name
        sizesLabel.text = "Размеры " + look.size
        discountPriceLabel.text = look.discountPrice + " руб"
        priceLabel.text = look.price + " руб"
        addToFavoriteButton.isHidden = look.isFavorite
        
        let urls = look.images.compactMap { $0.url }
        configureScrollView(with: urls)
        configurePageControl(with: urls)
    }
    
    func showAddToFavoriteButton(_ show: Bool) {
        addToFavoriteButton.isHidden = !show
    }
    
    // MARK: - UI Actions
    
    @IBAction func addToFavoriteButtonTapped(_ sender: Any) {
        delegate?.productSuccessViewAddToFavoriteButtonTapped(self)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        delegate?.productSuccessViewShareButtonTapped(self)
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension ScanProductSuccessView {
    
    fileprivate func commonInit() {
        let key = String(describing: ScanProductSuccessView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        prepareConstraints()
        innerView.layer.cornerRadius = 8
        innerView.clipsToBounds = true
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        self.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }
    
    fileprivate func configureScrollView(with urls: [String]) {
        let urlsCount = urls.count
    
        scrollView.isPagingEnabled = true
        scrollView.bounces = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.scrollsToTop = false
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(urlsCount),
                                        height: scrollView.frame.size.height)
        scrollView.delegate = self
        
        for i in 0 ..< urlsCount {
            let frame = CGRect(x: CGFloat(i) * scrollView.frame.size.width,
                               y: scrollView.frame.origin.y,
                               width: scrollView.frame.size.width,
                               height: scrollView.frame.size.height)
            let imageView = UIImageView(frame: frame)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            
            imageView.kf.setImage(with: URL(string: urls[i]),
                                  placeholder: UIImage(named: "look_placeholder"),
                                  options: nil,
                                  progressBlock: nil,
                                  completionHandler: { (image, _, _, _) in
                                    guard let image = image else { return }
                                    self.lookPreview = image
            })
            scrollView.addSubview(imageView)
        }
        
        if lookPreview == nil {
            lookPreview = UIImage(named: "look_placeholder")
        }
    }
    
    
    fileprivate func configurePageControl(with urls: [String]) {
        pageIndicator.numberOfPages = urls.count
        pageIndicator.currentPage = 0
    }
    
}

// MARK: - UIScrollViewDelegate

extension ScanProductSuccessView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPage = floor(scrollView.contentOffset.x / innerView.bounds.size.width);
        pageIndicator.currentPage = Int(currentPage)
    }
}
