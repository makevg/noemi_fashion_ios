//
//  FavoriteLookTableViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 15.10.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

protocol FavoriteLookTableViewCellDelegate: class {
    func shareButtonTapped(_ cell: FavoriteLookTableViewCell)
}

class FavoriteLookTableViewCell: BaseTableViewCell {
    
    // MARK: - Internal properties
    
    weak var delegate: FavoriteLookTableViewCellDelegate?
    var look: FavoriteLook?
    
    var lookImage: UIImage? {
        return lookImageView.image
    }
    
    // MARK: - Private properties
    
    @IBOutlet fileprivate weak var lookImageView: UIImageView!
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with look: FavoriteLook) {
        self.look = look
        
        if let imageUrl = look.imageUrl {
            lookImageView.kf.setImage(with: URL(string: imageUrl),
                                      placeholder: UIImage(named: "look_placeholder"),
                                      progressBlock: nil,
                                      completionHandler: nil)
        }
        
        if let date = look.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            dateLabel.text = dateFormatter.string(from: date as Date)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        delegate?.shareButtonTapped(self)
    }

}
