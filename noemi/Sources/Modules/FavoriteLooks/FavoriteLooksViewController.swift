//
//  FavoriteLooksViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 15.10.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class FavoriteLooksViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    private let provider = FavoriteLooksProvider.shared
    fileprivate var looks = [FavoriteLook]()
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		configureTableView()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		reloadData()
	}
	
	// MARK: - Private
	
	private func configureTableView() {
		tableView.dataSource = self
		tableView.tableFooterView = UIView()
	}
	
	private func reloadData() {
		looks = provider.getFavoriteLooks()
		showEmptyView(looks.isEmpty)
		tableView.reloadData()
	}
	
	private func showEmptyView(_ show: Bool) {
		if show {
			let emptyView = EmptyView.emptyView
			emptyView.emptyText = "Вы ничего не добавили в избранное"
			tableView.backgroundView = emptyView
		} else {
			tableView.backgroundView = nil
		}
	}

}

// MARK: - UITableViewDataSource

extension FavoriteLooksViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return looks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = FavoriteLookTableViewCell.identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier,
                                                 for: indexPath) as! FavoriteLookTableViewCell
        cell.configure(with: looks[indexPath.row])
        cell.delegate = self
        
        return cell
    }
    
}

// MARK: - FavoriteLookTableViewCell

extension FavoriteLooksViewController: FavoriteLookTableViewCellDelegate {
    
    func shareButtonTapped(_ cell: FavoriteLookTableViewCell) {
        guard let lookImage = cell.lookImage else { return }
        NoemiShareManager.shared.showShareActionSheet(with: lookImage, in: self)
    }
    
}
