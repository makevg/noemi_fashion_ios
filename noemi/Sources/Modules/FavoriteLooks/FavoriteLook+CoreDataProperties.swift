//
//  FavoriteLook+CoreDataProperties.swift
//  
//
//  Created by Maximychev Evgeny on 15.10.17.
//
//

import Foundation
import CoreData


extension FavoriteLook {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavoriteLook> {
        return NSFetchRequest<FavoriteLook>(entityName: "FavoriteLook")
    }

    @NSManaged public var uuid: Int32
    @NSManaged public var imageUrl: String?
    @NSManaged public var date: NSDate?

}
