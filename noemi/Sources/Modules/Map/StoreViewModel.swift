//
//  StoreViewModel.swift
//  noemi
//
//  Created by Maximychev Evgeny on 12.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation
import MapKit

class StoreViewModel: NSObject, MKAnnotation {
    
    var title: String?
    var descr: String
    var email: String
    var phone: String
    var work: String
    var coordinate: CLLocationCoordinate2D
    
    init(with store: Store) {
        title = store.name
        descr = store.address
        email = store.email
        phone = store.phone
        work = store.work
        
        let latitude = CLLocationDegrees(store.latitude)
        let longitude = CLLocationDegrees(store.longitude)
        coordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
    }
    
}
