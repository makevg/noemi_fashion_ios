//
//  ShopInfoView.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import CoreLocation

class ShopInfoView: UIView, LocationManagerProvider {
    
    // MARK: - Properties

    @IBOutlet fileprivate var mainView: UIView!
    @IBOutlet fileprivate weak var infoView: UIView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var locationLabel: UILabel!
    @IBOutlet fileprivate weak var distanceLabel: UILabel!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var phoneButton: ShadowButton!
    
    fileprivate var store: StoreViewModel?
    
    // MARK: - Init
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    // MARK: - UI Actions
    
    @IBAction func phoneButtonTapped(_ sender: Any) {
			guard let store = store else {
				return
			}
			
			UrlOpener.open("telprompt://" + store.phone)
    }
    
    // MARK: - Public
    
    func configure(with store: StoreViewModel) {
        self.store = store
        
        titleLabel.text = store.title
        locationLabel.text = store.descr
        timeLabel.text = store.work
        
        if let location = locationManager.location {
            let officeLocation = CLLocation(latitude: store.coordinate.latitude,
                                            longitude: store.coordinate.longitude)
            let distance = location.distanceInKilometersString(from: officeLocation)
            distanceLabel.text = distance + " км"
        }
    }
    
}

// MARK: - Extensions

// MARK: - Private

fileprivate extension ShopInfoView {
    
    fileprivate func commonInit() {
        let key = String(describing: ShopInfoView.self)
        Bundle.main.loadNibNamed(key, owner: self, options: nil)
        addSubview(mainView)
        configureView()
    }
    
    private func configureView() {
        prepareConstraints()
        prepareSubviews()
    }
    
    private func prepareConstraints() {
        mainView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
        addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options:[], metrics:nil, views:["view":mainView])
        )
    }
    
    private func prepareSubviews() {
        backgroundColor = UIColor.clear
        mainView.backgroundColor = UIColor.clear
        infoView.backgroundColor = UIColor.white
        infoView.layer.shadowOffset = CGSize(width: 0, height: 1)
        infoView.layer.shadowRadius = 1.5
        infoView.layer.shadowOpacity = 0.5
        phoneButton.layer.cornerRadius = phoneButton.frame.width / 2
        distanceLabel.text = ""
    }
    
}
