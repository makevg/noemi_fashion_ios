//
//  MapViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 28.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var mapView: MKMapView!
    @IBOutlet fileprivate weak var shopInfoView: ShopInfoView!
    
    fileprivate var storeViewModel: StoreViewModel?
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    // MARK: - Public
    
    func build(with store: Store) {
        let storeViewModel = StoreViewModel(with: store)
        self.storeViewModel = storeViewModel
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension MapViewController {
    
    fileprivate func configureController() {
        mapView.showsUserLocation = true
        
        guard let store = storeViewModel else { return }
        shopInfoView.configure(with: store)
        configureMap(with: store)
    }
    
    fileprivate func configureMap(with store: StoreViewModel) {
        mapView.addAnnotation(store)
        centerMapOnLocationCoordinate(store.coordinate)
    }
    
    fileprivate func centerMapOnLocationCoordinate(_ locationCoordinate: CLLocationCoordinate2D) {
			let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locationCoordinate, span: span)
        mapView.setRegion(region, animated: true)
    }
    
}
