//
//  LookBookInteractor.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import Foundation

class LookBookInteractor {
    
    // MARK: - Properties
    
    weak var presenter: LookBookPresenter!
    
    private let service = ServiceLocator.shared.looksService
    
    // MARK: - Public
    
	func obtainLookBooks(with category: String?, completion: @escaping ([LookBookItem]?, ServiceCode?, ServiceError?) -> ()) {
		service.getLookBookList(with: category, completion: completion)
	}
	
	func addProductToFavorite(with container: FavoriteContainer, completion: @escaping BaseServiceCompletion) {
		service.setFavoriteProduct(with: container, completion: completion)
	}
    
}
