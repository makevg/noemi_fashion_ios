//
//  LookBookCategoriesViewController.swift
//  noemi
//
//  Created by Максимычев Е.О. on 05/06/2018.
//  Copyright © 2018 Maximychev Evgeny. All rights reserved.
//

import UIKit

protocol LookBookCategoriesViewControllerDelegate: class {
	func lookBookCategoriesController(_ controller: LookBookCategoriesViewController,
																		didSelect category: LookBookCategory)
}

final class LookBookCategoriesViewController: BaseViewController {
	
	// MARK: - Properties

	@IBOutlet fileprivate weak var tableView: UITableView!
	private let service = ServiceLocator.shared.looksService
	
	fileprivate let cellIdentifier = "LookBookCategoryCell"
	fileprivate var categories = [LookBookCategory]()
	
	fileprivate lazy var cancelButton: UIBarButtonItem = {
		let button = UIBarButtonItem(title: "Отменить",
																 style: .plain,
																 target: self,
																 action: #selector(self.cancelButtonTapped(_:)))
		return button
	}()
	
	weak var delegate: LookBookCategoriesViewControllerDelegate?
	
	// MARK: - Lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		navigationItem.leftBarButtonItem = cancelButton
		configureTableView()
		obtainCategories()
	}
	
	// MARK: - Private
	
	@objc private func cancelButtonTapped(_ sender: UIBarButtonItem) {
		dismiss(animated: true, completion: nil)
	}
	
	private func configureTableView() {
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
		tableView.tableFooterView = UIView()
		tableView.dataSource = self
		tableView.delegate = self
	}
	
	private func obtainCategories() {
		service.getLookBookCategories { [weak self] categoriesList, code, error in
			guard let this = self else { return }
			
			guard let categoriesList = categoriesList,
				let serviceCode = code,
				error == nil else {
					UIAlertController.showErrorAlert(in: this)
					return
			}
			
			switch serviceCode {
			case .success:
				this.update(with: categoriesList)
			case .error:
				UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
			}
		}
	}
	
	private func update(with categories: [LookBookCategory]) {
		self.categories = categories
		tableView.reloadData()
	}

}

// MARK: - Extensions

// MARK: - UITableViewDataSource

extension LookBookCategoriesViewController: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return categories.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
		cell.textLabel?.text = categories[indexPath.row].name
		return cell
	}
	
}

// MARK: - UITableViewDelegate

extension LookBookCategoriesViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let category = categories[indexPath.row]
		delegate?.lookBookCategoriesController(self, didSelect: category)
		tableView.deselectRow(at: indexPath, animated: true)
		self.dismiss(animated: true, completion: nil)
	}
	
}
