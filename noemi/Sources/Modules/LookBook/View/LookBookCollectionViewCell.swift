//
//  LookBookCollectionViewCell.swift
//  noemi
//
//  Created by Maximychev Evgeny on 03.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import Kingfisher

protocol LookBookCollectionViewCellDelegate: class {
    func lookBookCellDidTapShareButton(_ cell: LookBookCollectionViewCell)
    func lookBookCellDidTapFavoriteButton(_ cell: LookBookCollectionViewCell)
}

class LookBookCollectionViewCell: BaseCollectionViewCell {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var lookImageView: UIImageView!
    @IBOutlet fileprivate weak var innerView: UIView!
    @IBOutlet fileprivate weak var favoriteButton: UIButton!
    
    weak var delegate: LookBookCollectionViewCellDelegate?
    var lookBook: LookBookItem?
    
    var lookImage: UIImage? {
        return lookImageView.image
    }
	
	var lookIsFavorite: Bool? {
		didSet {
			let isFavorite = lookIsFavorite ?? false
			let imageName = isFavorite ? "filledStar" : "notFilledStar"
			favoriteButton.setImage(UIImage(named: imageName), for: .normal)
		}
	}
    
    // MARK: - Override
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        clearSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureCell()
    }
    
    // MARK: - Public
    
    func configure(with lookBook: LookBookItem) {
        self.lookBook = lookBook
		let provider = FavoriteLooksProvider.shared
		lookIsFavorite = provider.lookIsFavorite(lookBook)
        lookImageView.kf.setImage(with: URL(string: lookBook.imageUrl),
                                  placeholder: UIImage(named: "look_placeholder"),
                                  progressBlock: nil,
                                  completionHandler: nil)
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension LookBookCollectionViewCell {
    
    fileprivate func configureCell() {
        innerView.layer.cornerRadius = 8
        lookImageView.layer.cornerRadius = 8
        lookImageView.layer.shadowOffset = CGSize(width: 0, height: 1)
        lookImageView.layer.shadowColor = UIColor.black.cgColor
        lookImageView.layer.shadowRadius = 3.5
        lookImageView.layer.shadowOpacity = 0.8
        
        clearSubviews()
    }
    
    fileprivate func clearSubviews() {
        lookImageView.image = nil
    }
    
}

// MARK: - Actions

extension LookBookCollectionViewCell {
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        delegate?.lookBookCellDidTapShareButton(self)
    }
    
    @IBAction func favoriteButtonTapped(_ sender: Any) {
        delegate?.lookBookCellDidTapFavoriteButton(self)
    }
    
}
