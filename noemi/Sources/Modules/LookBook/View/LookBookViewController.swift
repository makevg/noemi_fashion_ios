//
//  LookBookViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 03.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class LookBookViewController: BaseViewController {
    
    // MARK: - Properties
	
    @IBOutlet fileprivate weak var selectCategoryButton: UIButton!
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var activityIndicator: UIActivityIndicatorView!
    
    fileprivate var itemSize: CGSize {
        let bounds = UIScreen.main.bounds
        let itemWidth = bounds.width - 70
        let itemHeight = bounds.height - 160
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    fileprivate var presenter: LookBookPresenter!
    fileprivate var lookBooks = [LookBookItem]()
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.viewWillAppearEvent()
    }
    
	
	@IBAction func didTapSelectCategoryButton(_ sender: Any) {
		presenter.didTapSelectCategoryButton()
	}
	
	// MARK: - Public
    
    func build() {
        let presenter = LookBookPresenter()
        let interactor = LookBookInteractor()
        presenter.view = self
        presenter.interactor = interactor
        interactor.presenter = presenter
        self.presenter = presenter
    }
    
	func updateUI(with lookBooks: [LookBookItem], category: LookBookCategory?) {
		self.lookBooks = lookBooks
		showEmptyView(lookBooks.isEmpty)
		collectionView.reloadData()
		
		var buttonTitle = "Выберите категорию"
		if let categoryName = category?.name {
			buttonTitle = "Категория: \(categoryName)"
		}
		
		selectCategoryButton.setTitle(buttonTitle, for: .normal)
	}
    
    func showActivityIndicator(_ show: Bool) {
        if show {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }

}

// MARK: - Extensions

// MARK: - Private

fileprivate extension LookBookViewController {
    
    fileprivate func configureController() {
        build()
        configureCollectionView()
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = self
        let collectionViewLayout = SAHorizontalLinearFlowLayout(configuredWith: collectionView,
                                                                itemSize: itemSize,
                                                                minimumLineSpacing: 0)
        collectionViewLayout?.minimumScaleFactor = 0.9;
    }
	
	fileprivate func showEmptyView(_ show: Bool) {
		if show {
			let emptyView = EmptyView.emptyView
			emptyView.emptyText = "Ничего не найдено"
			collectionView.backgroundView = emptyView
		} else {
			collectionView.backgroundView = nil
		}
	}
    
}

// MARK: - UICollectionViewDataSource

extension LookBookViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lookBooks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LookBookCollectionViewCell.identifier,
                                                      for: indexPath) as! LookBookCollectionViewCell
        cell.configure(with: lookBooks[indexPath.row])
        cell.delegate = presenter
        return cell
    }
    
}
