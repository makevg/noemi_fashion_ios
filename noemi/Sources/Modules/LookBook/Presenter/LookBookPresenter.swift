//
//  LookBookPresenter.swift
//  noemi
//
//  Created by Maximychev Evgeny on 19.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class LookBookPresenter {
    
    // MARK: - Properties
    
    weak var view: LookBookViewController!
    var interactor: LookBookInteractor!
	
    fileprivate let shareManager = NoemiShareManager()
	fileprivate var selectedCategory: LookBookCategory?
    
    // MARK: - Public
    
    func viewWillAppearEvent() {
			obtainLooks(with: selectedCategory)
    }
	
	fileprivate func obtainLooks(with category: LookBookCategory?) {
		var categoryId = ""
		if let category = category {
			categoryId = "\(category.uuid)"
		}
		
		view.showActivityIndicator(true)
		interactor.obtainLookBooks(with: categoryId) { [weak self] (lookBooks, code, error) in
			guard let this = self else { return }
			
			guard let lookBooks = lookBooks,
				let serviceCode = code,
				error == nil else
			{
				UIAlertController.showErrorAlert(in: this.view)
				return
			}
			
			this.view.showActivityIndicator(false)
			switch serviceCode {
			case .success:
				this.view.updateUI(with: lookBooks, category: category)
			case .error:
				UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this.view)
			}
		}
	}
	
	func didTapSelectCategoryButton() {
		let vc = UIStoryboard.Looks.looksBooksCategoriesViewController
		vc.delegate = self
		let navigationVC = UINavigationController(rootViewController: vc)
		view.present(navigationVC, animated: true, completion: nil)
	}
    
}

// MARK: - Extensions

// MARK: - LookBookCategoriesViewControllerDelegate

extension LookBookPresenter: LookBookCategoriesViewControllerDelegate {
	
	func lookBookCategoriesController(_ controller: LookBookCategoriesViewController, didSelect category: LookBookCategory) {
		selectedCategory = category
		obtainLooks(with: category)
	}
	
}

// MARK: - LookBookCollectionViewCellDelegate

extension LookBookPresenter: LookBookCollectionViewCellDelegate {
    
    func lookBookCellDidTapShareButton(_ cell: LookBookCollectionViewCell) {
        guard let lookImage = cell.lookImage else { return }
        
        NoemiShareManager.shared.showShareActionSheet(with: lookImage, in: view)
    }
    
    func lookBookCellDidTapFavoriteButton(_ cell: LookBookCollectionViewCell) {
        guard let look = cell.lookBook else { return }
		
		let provider = FavoriteLooksProvider.shared
		if provider.lookIsFavorite(look) {
			provider.removeLook(look)
			cell.lookIsFavorite = false
		} else {
			provider.addLook(look)
			cell.lookIsFavorite = true
		}
    }
    
}
