//
//  ShareSaleViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ShareSaleViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var saleLabel: UILabel!
    private let service = ServiceLocator.shared.profileSrevice
    private let shareManager = NoemiShareManager()
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        saleLabel.text = ""
        service.getSaleCode { [weak self] (saleCode, code, error) in
            guard let this = self else { return }
            
            guard let saleCode = saleCode,
                let serviceCode = code,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch serviceCode {
            case .success:
                this.saleLabel.text = "\(saleCode)"
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
            }
        }
    }

    // MARK: - UI Actions
    
    @IBAction func shareSaleButtonTapped(_ sender: Any) {
        if let saleCode = saleLabel.text {
            let text = "\(saleCode)\nАктивируй код в приложении noemi и получи скидку в магазине. Подробнее на noemifashion.com"
            shareManager.showActivityController(with: [text], in: self)
        }
    }
    
    @IBAction func useSaleButtonTapped(_ sender: Any) {
        service.getSaleCode { [weak self] (saleCode, serviceCode, error) in
            guard let this = self else { return }
            
            guard let saleCode = saleCode,
                let serviceCode = serviceCode,
                error == nil else
            {
                UIAlertController.showErrorAlert(in: this)
                return
            }
            
            switch serviceCode {
            case .success:
                UIAlertController.showAlert(with: "Код для получения скидки", message: "\(saleCode)", in: this )
            case .error:
                UIAlertController.showAlert(with: "Ошибка", message: serviceCode.rawValue, in: this)
            }
        }
    }
    
}
