//
//  ShareBonusesViewController.swift
//  noemi
//
//  Created by Maximychev Evgeny on 27.06.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit

class ShareBonusesViewController: BaseViewController {
    
    // MARK: - Properties
    
    @IBOutlet fileprivate weak var phoneField: BaseTextField!
    @IBOutlet fileprivate weak var bonusesField: BaseTextField!
    
    private let service = ServiceLocator.shared.profileSrevice
    
    // MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneField.becomeFirstResponder()
    }
    
    // MARK: - UI Actions

    @IBAction func sendButtonTapped(_ sender: Any) {
        let phone = phoneField.text ?? ""
        let points = bonusesField.text ?? ""
        
        if !phone.isEmpty && !points.isEmpty {
            let container = SendPointsContainer(phone: "+7" + phone, points: Int(points)!)
            service.sendPoints(with: container, completion: { [weak self] (info, error) in
                guard let this = self else { return }
                
                guard let responseInfo = info, error == nil else {
                    UIAlertController.showErrorAlert(in: this)
                    return
                }
                
                switch responseInfo.code {
                case .success:
                    UIAlertController.showAlert(with: "Внимание", message: "Успешно", in: this)
                case .error:
                    UIAlertController.showAlert(with: "Внимание", message: responseInfo.message ?? "", in: this)
                }
            })
        } else {
            UIAlertController.showAlert(with: "Внимание", message: "Заполните поля", in: self)
        }
    }
    
}
