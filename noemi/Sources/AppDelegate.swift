//
//  AppDelegate.swift
//  noemi
//
//  Created by Maximychev Evgeny on 23.05.17.
//  Copyright © 2017 Maximychev Evgeny. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    
    var window: UIWindow?
    var locationManager = CLLocationManager()
    private let settings = Settings.shared
		private let authService = ServiceLocator.shared.authService
		private let messaging = Messaging.messaging()
    
    // MARK: - Private
    
    private func configureNavBar() {
        let appearance = UINavigationBar.appearance()
        let barColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        appearance.barTintColor = barColor
        appearance.tintColor = UIColor.black
        appearance.isTranslucent = false
			appearance.titleTextAttributes = [NSAttributedString.Key.font: UIFont.noemiFont(with: 28),
																				NSAttributedString.Key.foregroundColor:UIColor.red]
        appearance.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        appearance.setBackgroundImage(UIImage(named: ""), for: .default)
        UIApplication.shared.statusBarView?.backgroundColor = barColor
        
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "back")
    }
    
    private func prepareSettings() {
        if settings.isFirstLaunch {
            settings.clearAccessToken()
            settings.isFirstLaunch = false
        }
    }
	
	@available(iOS 10.0, *)
	func registerForPushNotifications() {
		guard authService.userAuthorized() else {
			return
		}
		
		let notificationCenter = UNUserNotificationCenter.current()
		notificationCenter.delegate = self
		notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) {
				[weak self] granted, error in
				guard let self = self else { return }
				print("Permission granted: \(granted)")
				
				guard granted else { return }
				
				self.getNotificationSettings()
		}
	}
	
	@available(iOS 10.0, *)
	func unregisterForPushNotifications() {
		UNUserNotificationCenter.current().delegate = nil
		UIApplication.shared.unregisterForRemoteNotifications()
	}
	
	@available(iOS 10.0, *)
	func getNotificationSettings() {
		UNUserNotificationCenter.current().getNotificationSettings { settings in
			print("Notification settings: \(settings)")
			guard settings.authorizationStatus == .authorized else { return }
			DispatchQueue.main.async {
				UIApplication.shared.registerForRemoteNotifications()
				FirebaseApp.configure()
			}
		}
	}
	
    // MARK: - UIApplicationDelegate

    func application(_ application: UIApplication,
										 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
			configureNavBar()
			prepareSettings()
			
			if #available(iOS 10.0, *) {
				registerForPushNotifications()
			}
			
			Fabric.with([Crashlytics.self])
			RateUsManager.checkAndAskForReview()
			
			return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}

    func applicationDidEnterBackground(_ application: UIApplication) {}

    func applicationWillEnterForeground(_ application: UIApplication) {}

    func applicationDidBecomeActive(_ application: UIApplication) {}

    func applicationWillTerminate(_ application: UIApplication) {}
	
	func application(_ application: UIApplication,
									 didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		messaging.apnsToken = deviceToken
		messaging.subscribe(toTopic: "news") { error in
			print("Subscribed to news topic")
		}
	}
	
	func application(_ application: UIApplication,
									 didFailToRegisterForRemoteNotificationsWithError error: Error) {
		print("Failed to register: \(error)")
	}
	
	func application(_ application: UIApplication,
									 didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
		print(userInfo)
	}
	
	func application(_ application: UIApplication,
									 didReceiveRemoteNotification userInfo: [AnyHashable: Any],
									 fetchCompletionHandler completionHandler:
		@escaping (UIBackgroundFetchResult) -> Void) {
		print(userInfo)
		completionHandler(.newData)
	}

}

// MARK: - UNUserNotificationCenterDelegate

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
	
	func userNotificationCenter(_ center: UNUserNotificationCenter,
															willPresent notification: UNNotification,
															withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler([.alert, .sound, .badge])
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter,
															didReceive response: UNNotificationResponse,
															withCompletionHandler completionHandler: @escaping () -> Void) {
		if let info = response.notification.request.content.userInfo as? [String: AnyObject],
			let id = info["id"] as? String,
			let newsId = Int(id) {
			print("newsId: \(newsId)")
			
			if let rootViewController = window?.rootViewController {
				let vc = UIStoryboard.News.newsCardViewController
				vc.configure(with: newsId)
				vc.presentModal(presentingController: rootViewController)
			}
		}
		
		completionHandler()
	}
}
